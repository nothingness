/*
 *  Nothingness - massive multiplayer space action
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "audio.h"

Sound_Sample *g_sample;				/* Zvuk */
Uint32 g_pos;					/* Pozice pri prehravani */

/*
 * Audio callback funkce, posila data do streamu
 */

void AudioCallback (void *unused, Uint8 *stream, int len)
{
	/* Ukazatel na cast, kde se ma zacit prehravat */
	Uint8 *wave_ptr = (Uint8 *)g_sample->buffer + g_pos;

	/* Delka zvuku do konce */
	int wave_left = g_sample->buffer_size - g_pos;

	/* Zbyvajici delka je mensi nez pozadovana */
	/* Cyklus, protoze cely zvuk muze byt kratsi */
	while(wave_left <= len)
	{
		/* Posle data na zvukovou kartu */
		SDL_MixAudio(stream, wave_ptr, wave_left, SDL_MIX_MAXVOLUME);

		/* Posune se o prave zapsana data */
		stream += wave_left;
		len -= wave_left;

		/* Od zacatku zvuku */
		wave_ptr = (Uint8 *)g_sample->buffer;
		wave_left = g_sample->buffer_size;
		g_pos = 0;
	}

	/* Je jistota, ze zbyvajici cast zvuku je delsi nez pozadovana */
	SDL_MixAudio(stream, wave_ptr, len, SDL_MIX_MAXVOLUME);
	g_pos += len;
}

bool PlayAudio (char *filename)
{
	/* Nastaveni audia */
	SDL_AudioSpec desired, obtained;
	desired.freq = 22050;/* 16-bit. stereo na 22 kHz */
	desired.format = AUDIO_S16;
	desired.channels = 2;
	desired.samples = 512;/* Vhodne pro hry */
	desired.callback = AudioCallback;
	desired.userdata = NULL;

	/* Otevre audio zarizeni */
	if(SDL_OpenAudio(&desired, &obtained) == -1)
	{
		fprintf(stderr, "Unable to open audio: %s\n", SDL_GetError());
		return 0;
	}

	/* Loading audia a dekodovani (vse najednou) */
	Sound_AudioInfo info;
	info.channels = obtained.channels;
	info.format = obtained.format;
	info.rate = obtained.freq;

	g_sample = Sound_NewSampleFromFile(filename, &info, 512);
	if(g_sample == NULL)
	{
		fprintf(stderr, "Unable to load sound: %s\n", Sound_GetError());
		return 0;
	}

	/* Dekoduje cely zvuk najednou */
	Sound_DecodeAll(g_sample);
	if(g_sample->flags & SOUND_SAMPLEFLAG_ERROR)
	{
		fprintf(stderr, "Unable to decode sound: %s\n",
				Sound_GetError());
		return 0;
	}

	/* Zacne prehravat */
	SDL_PauseAudio(0);

	return 1;
}

bool InitAudio ()
{
	if ( SDL_Init( SDL_INIT_AUDIO ) < 0) /* inicializace zvuku */
	{
		fprintf( stderr, "Audio initialization failed: %s\n",
			 SDL_GetError( ) );

		return 0;
	}
	
	/* Inicializace SDL_sound */
	if(Sound_Init() == 0)
	{
		fprintf(stderr, "Unable to initialize SDL_sound: %s\n",
			Sound_GetError());

		return 0;
	}
	
	// Hraj
	PlayAudio ("data/music/music.mp3");

	return 1;
}

