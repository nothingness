/*
 *  Nothingness - massive multiplayer space action
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <SDL.h>
#include "scene.h"
#include "3ds/3dsloader.h"
#include "network.h"
#include "ship.h"
#include "font.h"
#include "config.h"
#include "proto.h"
#include "cache.h"

bool bTextured = true;
bool bLightGL = true;
bool bAnimated = true;

int verbose = 2;
int renderMode = 0;
int frameRate = 7;

bool allowfire = true;

float mouse_y = 0, mouse_u = 0;

/* Pomocne prov vypocet FPS*/
float g_fps = 1.0f;			/* Aktualni FPS*/
Uint32 g_last_time = 0;			/* Cas minuleho prekresleni*/

/* This is our SDL surface */
SDL_Surface *surface;

GLfloat LightAmbient[]= { 0.9f, 0.9f, 0.9f, 1.0f };// Okolni svetlo
GLfloat LightDiffuse[]= { 1.0f, 1.0f, 1.0f, 1.0f };// Prime svetlo
GLfloat LightPosition[]= { -1.0f, -1.0f, -1.0f, 1.0f };// Pozice svetla

typedef struct tagOBJECT
{
	unsigned type;
	float x, y, z;
	float u, v;
	float size;
	model3ds_t *model;
} OBJECT;

OBJECT *obj;

unsigned int numobjects = 0;

typedef struct tagVERTEX
{
	float x, y, z;
	float u, v;
} VERT;

extern ship_t ship_list; // structure of all players
extern ship_ammo_t ship_ammo_list;
extern CONFIG config;

int view = 1;

/* timer */
SDL_TimerID ship_timer;

/* constant used for converting to radians */
const float piover180 = 0.0174532925f;

/* textures */
GLuint img_logo;
GLuint img_font;
GLuint img_particle;
GLuint img_particle2;
GLuint img_particle3;

// Constructors
void Update ();
bool LoadObjects (char *filename);
//t3DModel *Load3ds (char *filename);
void drawmodel (model3ds_t *object);
Uint32 ShipTimer (Uint32 interval, void *param);
void DrawSprite (double x, double y, double z,double dimensione,float r,float g,float b,float iox,float ioy,float ioz,GLuint texture);
///////////////


/* vector math */
typedef struct
{
	double x;
	double y;
	double z;
} Vector;

Vector VectorNormalize( Vector v )
{
	Vector vresult;
	double l = VectorLength( v );

	vresult.x = v.x/l;
	vresult.y = v.y/l;
	vresult.z = v.z/l;

	return vresult;
}

Vector VectorMultiply( Vector v1, Vector v2 )
{
	Vector vresult;

	vresult.x = v1.y * v2.z - v1.z * v2.y;
	vresult.y = v1.z * v2.x - v1.x * v2.z;
	vresult.z = v1.x * v2.y - v1.y * v2.x;

	return vresult;
}

Vector VectorScalarMultiply( Vector v, double s )
{
	Vector vresult;

	vresult.x = v.x * s;
	vresult.y = v.y * s;
	vresult.z = v.z * s;

	return vresult;
}

Vector VectorDiff( Vector v1, Vector v2 )
{
	Vector vresult;

	vresult.x = v1.x - v2.x;
	vresult.y = v1.y - v2.y;
	vresult.z = v1.z - v2.z;

	return vresult;
}	

//###############################################

int InitGame ()
{
	LoadObjects (OBJECT_WORLD_PATH);

        loadmodel (SHIP_MODEL_PATH "esta-eco/cockpit.3ds");

	me->energy = 1024;
	me->speed = 0;

	ship_timer = SDL_AddTimer (250, ShipTimer, NULL);  

	return 1;
}

/* vytvori mlhu */

void InitFog()
{

    	const GLfloat	fogColor[4] = {1.0f, 0.4f, 0.4f, 0.8f};		// Fog Colour 

	glFogi(GL_FOG_MODE, GL_LINEAR);					// Fog Fade Is Linear
	glFogfv(GL_FOG_COLOR, fogColor);				// Set The Fog Color
	glFogf(GL_FOG_START,  100.0f);					// Set The Fog Start
	glFogf(GL_FOG_END,    1000.0f);					// Set The Fog End
	glHint(GL_FOG_HINT, GL_NICEST);					// Per-Pixel Fog Calculation
	
	glFogf(GL_FOG_DENSITY, 20.0f);
	glEnable(GL_FOG);

}

/* general OpenGL initialization function */
int InitGL (GLvoid)
{
	/* Load in the texture */
	if (!(img_logo = loadtexture ("./data/hud/logo.bmp")))
		return 0;

	if (!(img_font = loadtexture ("./data/font/font.bmp")))
		return 0;

	if (!(img_particle = loadtexture ("./data/textures/particle.bmp")))
		return 0;

	if (!(img_particle2 = loadtexture ("./data/textures/particle2.bmp")))
		return 0;

	if (!(img_particle3 = loadtexture ("./data/textures/particle3.bmp")))
		return 0;

	/* Enable Texture Mapping */
	glEnable (GL_TEXTURE_2D);

	/* Enable smooth shading */
	glShadeModel (GL_SMOOTH);

	/* Set the background black */
	glClearColor (0.7f, 0.7f, 0.9f, 0.0f);
	//glClearColor (0.0f, 0.0f, 0.0f, 0.0f);

	/* Depth buffer setup */
	glClearDepth (1.0f);

	/* Enables Depth Testing */
	glEnable (GL_DEPTH_TEST);

	/* The Type Of Depth Test To Do */
	glDepthFunc (GL_LEQUAL);

	/* Really Nice Perspective Calculations */
	// glHint (GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	/* Setup The Ambient Light */
	glLightfv (GL_LIGHT1, GL_AMBIENT, LightAmbient);

	/* Setup The Diffuse Light */
	glLightfv (GL_LIGHT1, GL_DIFFUSE, LightDiffuse);

	/* Position The Light */
	glLightfv (GL_LIGHT1, GL_POSITION, LightPosition);

	glLightModelf (GL_LIGHT_MODEL_TWO_SIDE, 1.0);

	connect_toserver ();	// NETWORK :P
	/* Enable Light One */
	//glEnable( GL_LIGHT1 );

	//glEnable(GL_LIGHTING); 

        //InitFog ();

	InitFont ();

	InitGame ();

	return( TRUE );
}

/* Read In A String */
void readstr (FILE *f, char *string)
{
	/* Start A Loop */
	do {
		/* Read One Line */
		fgets (string, 255, f);
        } while ((string[0] == '/') || (string[0] == '\n'));

	return;
}

bool LoadObjects (char *filename)
{
	unsigned type;
	float x, y, z, u, v, size;
	char modelfile[80];
	FILE *filein;
	char oneline[255];

	filein = fopen (filename, "rt");	 // File To Load World Data From

	if (!filein) {
		printf ("ERROR -> world file %s not exist !\n", filename);
		return 0;
	}

	printf ("INFO -> Loading world file %s with all objects ..\n", filename);

	readstr (filein,oneline);
	sscanf (oneline, "NUMOBJECTS %d\n", &numobjects);

	obj = (OBJECT *) malloc (numobjects * sizeof (OBJECT));

	for (int loop = 0; loop < (signed) numobjects; loop ++) {
		readstr (filein, oneline);

		sscanf (oneline, "%d %f %f %f %f %f %f %s", &type, &x, &y, &z, &u, &v, &size, modelfile);

		obj[loop].type = type;
		obj[loop].x = x;
		obj[loop].y = y;
		obj[loop].z = z;
		obj[loop].u = u;
		obj[loop].v = v;
		obj[loop].size = size;

		char mdl[64];

		switch (type) {
			case OBJECT_TYPE_MODEL:
				sprintf (mdl, OBJECT_MODEL_PATH "%s.3ds", modelfile);

				printf ("OBJECT -> Loading %s model\n", mdl);

				obj[loop].model = loadmodel (mdl);

				sprintf (mdl, OBJECT_MODEL_PATH "%s.bmp", modelfile);

				obj[loop].model->texture = loadtexture (mdl);
				break;
			case OBJECT_TYPE_SUN:
				sprintf (mdl, OBJECT_TEXTURE_PATH "%s.bmp", modelfile);

				//obj[loop].model->id_texture = loadtexture (mdl);
				break;
			case OBJECT_TYPE_ASTEROID:
				sprintf (mdl, OBJECT_MODEL_PATH "%s.3ds", modelfile);

				printf ("OBJECT -> Loading %s asteroid model\n", mdl);

				obj[loop].model = loadmodel (mdl);

				sprintf (mdl, OBJECT_MODEL_PATH "%s.bmp", modelfile);

				obj[loop].model->texture = loadtexture (mdl);
				break;
			case OBJECT_TYPE_STAR:
				sprintf (mdl, OBJECT_TEXTURE_PATH "%s.bmp", modelfile);

				printf ("OBJECT -> Loading %s star\n", mdl);

				//obj[loop].model->id_texture = loadtexture (mdl);
				break;
			case OBJECT_TYPE_PLANET:
				sprintf (mdl, OBJECT_MODEL_PATH "%s.3ds", modelfile);

				printf ("OBJECT -> Loading %s planet model\n", mdl);

				obj[loop].model = loadmodel (mdl);

				sprintf (mdl, OBJECT_MODEL_PATH "%s.bmp", modelfile);

				obj[loop].model->texture = loadtexture (mdl);
				break;
		}
	}

	fclose (filein);

	return 1;
}

void DrawObjects ()
{
	for (int loop = 0; loop < (signed) numobjects; loop ++) {
		glPushMatrix ();

			glTranslatef (obj[loop].x, obj[loop].y, obj[loop].z);
			glRotatef (obj[loop].u, 0, 1, 0);
			glRotatef (obj[loop].v, 1, 0, 0);

			glScalef (obj[loop].size, obj[loop].size, obj[loop].size);

			switch (obj[loop].type) {
				case OBJECT_TYPE_MODEL:
					drawmodel (obj[loop].model);
					break;
				case OBJECT_TYPE_SUN:
					//glSpharef (5, 5, 5);
					break;
				case OBJECT_TYPE_ASTEROID:
					obj[loop].u += (float) ((float) 2.0f / (float) g_fps);
					obj[loop].v += (float) ((float) 3.0f / (float) g_fps);
					drawmodel (obj[loop].model);
					break;
				case OBJECT_TYPE_STAR:
					
					break;
				case OBJECT_TYPE_PLANET:
					obj[loop].u += (float) ((float) 0.1f / (float) g_fps);
					drawmodel (obj[loop].model);
					break;
			}


		glPopMatrix ();
	}

}

unsigned char fire_side = 0;
unsigned FireAmmo (ship_t *owner, unsigned char type)
{
	/* energie nesmi klesnout pod 10 % pro strileni */
	if (owner->energy < 10)
		return 0;

	if (!allowfire)
		return 0;

	allowfire = false;

	ship_ammo_t *ammo = ship_ammo_register (owner);

	if (!ammo)
		return 0;

	switch (type) {
		case AMMO_TYPE_BULLET:
			ammo->lifetime = 10;
			ammo->speed = 3 + owner->speed;
			owner->energy -= 1;
			break;
		case AMMO_TYPE_PLASMA:
			ammo->lifetime = 15;
			ammo->speed = 10 + owner->speed;
			owner->energy -= 10;
			break;
		case AMMO_TYPE_EMP:
			ammo->lifetime = 5;
			ammo->speed = 20 + owner->speed;
			owner->energy -= 30;
			break;
		case AMMO_TYPE_LASER:
			ammo->lifetime = 25;
			ammo->speed =  + owner->speed;
			owner->energy -= 50;
			break;
	}

	ammo->type = type;
	ammo->xpos = owner->xpos;
	ammo->ypos = owner->ypos;
	ammo->zpos = owner->zpos;
	ammo->yrot = owner->yrot;
	ammo->urot = owner->urot;

	/* efekt, kdy jsou strely vystreleny pokazde z jine strany lode */
	if (!fire_side) {
		ammo->xpos -= (float) sin (ammo->yrot-90 * piover180) * 0.05f;
		ammo->zpos -= (float) cos (ammo->yrot-90 * piover180) * 0.05f;

		fire_side = 1;
	} else {
		ammo->xpos -= (float) sin (ammo->yrot+90 * piover180) * 0.05f;
		ammo->zpos -= (float) cos (ammo->yrot+90 * piover180) * 0.05f;

		fire_side = 0;
	}

	/* posleme na server info o vystrelene strele jen tehdy, zdali byla strela vystrelena z nasi lode */
	if (owner == me)
		proto_fire (ammo);
  
	return 1;
}


void DrawAmmoPlasma (ship_ammo_t *ammo)
{
	unsigned i;

	for (i = 0; i < 8; i ++) {
		ammo->xpos -= (float) sin (ammo->yrot * piover180) * (i*0.02f/g_fps);
		ammo->zpos -= (float) cos (ammo->yrot * piover180) * (i*0.02f/g_fps);
		ammo->ypos += (float) sin (ammo->urot * piover180) * (i*0.02f/g_fps);

		DrawSprite (ammo->xpos, -ammo->ypos, ammo->zpos, 0.03, (float) i / (float) 16, 0.3f, 0.1f, me->xpos, -me->ypos, me->zpos, img_particle2);
	}

}

float emp_bump;
void DrawAmmoEMP (ship_ammo_t *ammo)
{
	unsigned i;

	for (i = 0; i < 32; i ++) {
		ammo->xpos -= (float) cos (emp_bump * piover180) * (0.5f/g_fps);
		ammo->zpos -= (float) sin (emp_bump * piover180) * (0.5f/g_fps);
		//ammo->ypos += (float) cos (emp_bump * piover180) * (1.1f/g_fps);

		DrawSprite (ammo->xpos, -ammo->ypos, ammo->zpos, 0.02, 0,(float) i / (float) 16, 1, me->xpos, -me->ypos, me->zpos, img_particle);

		emp_bump ++;

		if (emp_bump > 360)
			emp_bump = 1;
	}
}

float laser_bump;
void DrawAmmoLaser (ship_ammo_t *ammo)
{
	unsigned i;

	for (i = 0; i < 128; i ++) {
		ammo->xpos -= (float) sin (ammo->yrot * piover180) * (i*0.05f/g_fps);
		ammo->zpos -= (float) cos (ammo->yrot * piover180) * (i*0.05f/g_fps);
		ammo->ypos += (float) sin (ammo->urot * piover180) * (i*0.05f/g_fps);

		DrawSprite (ammo->xpos, -ammo->ypos, ammo->zpos, 0.01, ((float) i / (float) 16)+0.3f, ((float) i / (float) 16)+0.1f, 0, me->xpos, -me->ypos, me->zpos, img_particle3);

		laser_bump ++;

		if (laser_bump > 360)
			laser_bump = 1;
	}

}

void DrawAmmo ()
{
	ship_ammo_t *ammo;

	for (ammo = ship_ammo_list.next; ammo != &ship_ammo_list; ammo = ammo->next) {
		if (!ammo)
			break;

		if (!ammo->lifetime) {
			ship_ammo_delete (ammo);
			break;
		}

		ammo->xpos -= (float) sin (ammo->yrot * piover180) * (ammo->speed/g_fps);
		ammo->zpos -= (float) cos (ammo->yrot * piover180) * (ammo->speed/g_fps);
		ammo->ypos += (float) sin (ammo->urot * piover180) * (ammo->speed/g_fps);

		switch (ammo->type) {
			case AMMO_TYPE_BULLET:
				/* zpomaleni rychlosti dobou letu */
				if (ammo->speed >= 0.1f)
					ammo->speed -= ((float) ammo->lifetime* (float) 0.01f)/g_fps;

				DrawSprite (ammo->xpos, -ammo->ypos, ammo->zpos, 0.002, 1,1,1, me->xpos, -me->ypos, me->zpos, img_particle);
				break;
			case AMMO_TYPE_PLASMA:
				DrawAmmoPlasma (ammo);
				break;
			case AMMO_TYPE_EMP:
				DrawAmmoEMP (ammo);
				break;
			case AMMO_TYPE_LASER:
				DrawAmmoLaser (ammo);
				break;
		}
	}
}

extern ship_t ship_list;
void DrawShips ()
{
	ship_t *ship;

	for (ship = ship_list.next; ship != &ship_list; ship = ship->next) {
		if (!ship->id) {
			ship_del (ship);
			break;
		}

		ship->xpos -= (float) sin (ship->yrot * piover180) * (ship->speed/g_fps);
		ship->zpos -= (float) cos (ship->yrot * piover180) * (ship->speed/g_fps);
		ship->ypos += (float) sin (ship->urot * piover180) * (ship->speed/g_fps);

		glPushMatrix ();

		glTranslatef (ship->xpos, -ship->ypos, ship->zpos);
		glRotatef (ship->yrot, 0, 1, 0);
		glRotatef (-ship->urot-90, 1, 0, 0);

		glScalef (0.1f, 0.1f, 0.1f);
		drawmodel (ship->cockpit.model);

		glPopMatrix ();
	}
}

void drawmodel (model3ds_t *object)
{
	if (!object)
		return;

	draw3ds (object);
}
/*
int Collision3ds (GLfloat xpos, GLfloat zpos, GLfloat x1, GLfloat z1, GLfloat x2, GLfloat z2)
{

	GLfloat xtrans = xpos;
	GLfloat ztrans = zpos;
	GLfloat tolerance = 0.1;
	GLuint kolizi = 0;
	
	if ((xtrans <= x1+tolerance && xtrans >= x2-tolerance) || (xtrans >= x1-tolerance && xtrans <= x2+tolerance))
		kolizi++;

	if ((ztrans >= z1-tolerance && ztrans <= z2+tolerance) || (ztrans <= z1+tolerance && ztrans >= z2-tolerance)) 
		kolizi++;
	
	if (kolizi > 1) 
		return 1;

	return 0;
}

int Coll3ds (t3DModel *g_model, VERT vert)
{
	float xpos, zpos;

	xpos = me->xpos - vert.x;
	zpos = me->zpos - vert.z;

		// Since we know how many objects our model has, go through each of them.
		for (int i = 0; i < g_model->numOfObjects; i ++) {
			// Make sure we have valid objects just in case. (size() is in the vector class)
			if (g_model->pObject.size() <= 0)
				break;
		
			// Get the current object that we are displaying
			t3DObject *pObject = &g_model->pObject[i];

		
			// Go through all of the faces (polygons) of the object and draw them
			for(int j = 0; j < pObject->numOfFaces; j++)
			{
				float x_m1, y_m1, z_m1, x_m2, y_m2, z_m2, x_m3, y_m3, z_m3; // 3d and texture coordinates

				// Go through each corner of the triangle and draw it.
				for(int whichVertex = 0; whichVertex < 3; whichVertex++)
				{
					// Get the index for each point of the face
					int index = pObject->pFaces[j].vertIndex[whichVertex];
		
					if (whichVertex == 0) {
						x_m1 = pObject->pVerts[ index ].x;
						y_m1 = pObject->pVerts[ index ].y;
						z_m1 = pObject->pVerts[ index ].z;
					} else if (whichVertex == 1) {
						x_m2 = pObject->pVerts[ index ].x;
						y_m2 = pObject->pVerts[ index ].y;
						z_m2 = pObject->pVerts[ index ].z;
					} else if (whichVertex == 2) {
						x_m3 = pObject->pVerts[ index ].x;
						y_m3 = pObject->pVerts[ index ].y;
						z_m3 = pObject->pVerts[ index ].z;
					}
					// Pass in the current vertex of the object (Corner of current face)
					//glVertex3f(pObject->pVerts[ index ].x, pObject->pVerts[ index ].y,
					//		pObject->pVerts[ index ].z);
				}

				if (((Collision3ds(xpos, zpos, x_m1, z_m1, x_m2, z_m2) || Collision3ds(xpos, zpos, x_m1, z_m1, x_m3, z_m3) || Collision3ds(xpos, zpos, x_m2, z_m2, x_m3, z_m3))) && !(y_m1 == y_m2 && y_m1 == y_m3))
				{
					return 1;
				}
			}
		
		}

	return 0;
}*/

//#################################
//------------ masking operations #
//#################################

void DrawIcon (GLuint texture, float r, float g, float b,float x, float y, float sizeX, float sizeY)
{	
//	glCallList(ortho_list);

	glPushMatrix ();

	if(sizeX<0)
		sizeX=0;
	if(sizeY<0)
		sizeY=0;
	
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_LIGHTING);
	
	glEnable(GL_BLEND);											
	glBlendFunc(GL_ONE,GL_ONE);
	
	glTranslatef(-0.6f,-0.5f,-1);
	glScalef(1.2f,1,1);
	
	glBindTexture(GL_TEXTURE_2D,  texture);
	
	glBegin(GL_QUADS);
	glColor4f(r, g, b, 1);
		glTexCoord2f(0.0f, 1.0f);	glVertex3f(x, y,0);   
		glTexCoord2f(0.0f, 0.0f);	glVertex3f(x, y+sizeY,0);
		glTexCoord2f(1.0f, 0.0f);	glVertex3f(x+sizeX, y+sizeY,0);
		glTexCoord2f(1.0f, 1.0f);	glVertex3f(x+sizeX, y,0);
	glEnd();
	
	glDisable(GL_BLEND);
	glEnable(GL_DEPTH_TEST);
	
	//	glCallList(perspective_list);

	glPopMatrix ();
}

void DrawSprite (double x, double y, double z, double dimensione, float r, float g, float b, float iox, float ioy, float ioz, GLuint texture)
{
	glPushMatrix();


	Vector Position;
	Vector MyPosition;
	Position.x = x;
	Position.y = y;
	Position.z = z;

	MyPosition.x=iox;
	MyPosition.y=ioy;
	MyPosition.z=ioz;
	Vector sight = VectorDiff(MyPosition, Position);

	Vector cz;
	cz.x = 0;
	cz.y = 0;
	cz.z = 1;

	Vector cross1 = VectorMultiply( sight, cz );
	Vector cross2 = VectorMultiply( sight, cross1 );

	cross1 = VectorNormalize(cross1);
	cross2 = VectorNormalize(cross2);

	cross1 = VectorScalarMultiply(cross1, dimensione);
	cross2 = VectorScalarMultiply(cross2, dimensione);


	glColor3f(r,g,b);
	glEnable (GL_BLEND);
	glBlendFunc( GL_ONE,GL_ONE);
	glDepthMask (GL_FALSE);
	//glDisable(GL_DEPTH_TEST);
	glBindTexture( GL_TEXTURE_2D, texture );
	glBegin(GL_QUADS);
		glTexCoord2d( 0.0, 0.0 );
		glVertex3d( Position.x + cross1.x, Position.y + cross1.y, Position.z + cross1.z);
		glTexCoord2d( 1.0, 0.0 );
		glVertex3d( Position.x - cross2.x, Position.y - cross2.y, Position.z - cross2.z);
		glTexCoord2d( 1.0, 1.0 );
		glVertex3d( Position.x - cross1.x, Position.y - cross1.y, Position.z - cross1.z);
		glTexCoord2d( 0.0, 1.0 );
		glVertex3d( Position.x + cross2.x, Position.y + cross2.y, Position.z + cross2.z);
	glEnd();

	glDisable (GL_BLEND);
	glDepthMask (GL_TRUE);

	glPopMatrix();
}

/* ################################################ */

void DrawHud ()
{

	DrawIcon (img_logo, 1,1,1, -0.5f, 1.3f, 0.256f, 0.256f); // Draw Logo

	// health bar
	int i = 1;
	/*for (i = 1; i <= me->health*2; i++)
		DrawIcon (img_particle2,(float)20/i,(float)i/200,0, -0.13f+((0.0122f/2)*i), -0.02f, 0.02f, 0.06f);*/

	// frag bar
	for (i = 1; i <= me->energy; i++)
		DrawIcon (img_particle2, (float)20/i,(float)i/200,0, -0.6f+((0.0124f/6)*i), -0.5f, 0.02f, 0.06f);
	
	glTranslatef (0.0f, 0.0f, -0.1);	// posune text do popredi
 
	glDisable (GL_TEXTURE_2D);

	glColor3f (0.5f, 0.6f, 0.3f);

	glRasterPos2f( 0.09f, 0.09f);
		glPrint( "fps: %.2f", g_fps);
	glRasterPos2f( -0.09f, -0.09f);

	glRasterPos2f( -0.095f, -0.09f);
		glPrint( "pos: %f / %f / %f / speed: %f / energy: %d", me->xpos, me->ypos, me->zpos, me->speed, me->energy);
	glRasterPos2f( 0.095f, 0.09f);

	glEnable (GL_TEXTURE_2D);
}

/* Here goes our drawing code */
void DrawGLScene()
{
	/* Floating Point For Temp X, Y, Z, U And V Vertices */
	//GLfloat x_m, y_m, z_m, u_m, v_m;
	/* Used For Player Translation On The X Axis */
	GLfloat xtrans = -me->xpos;
	/* Used For Player Translation On The Z Axis */
	GLfloat ztrans = -me->zpos;
	/* Used For Bouncing Motion Up And Down */
	GLfloat ytrans = me->ypos;
	/* 360 Degree Angle For Player Direction */
	GLfloat sceneroty = 360.0f - me->yrot;

	GLfloat scenerotv = (((mouse_y) - (me->yrot))*10*me->speed) / g_fps;

	/* Clear The Screen And The Depth Buffer */
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// Clear The Screen And The Depth Buffer
	glLoadIdentity ();

	glRotatef (me->urot, 1.0f, 0.0f , 0.0f );

	glRotatef (-scenerotv, 0.0f, 0.0f , 1.0f );

	/* Rotate Depending On Direction Player Is Facing */
	glRotatef (sceneroty, 0.0f, 1.0f , 0.0f );

	/* Translate The Scene Based On Player Position */
	glTranslatef( xtrans, ytrans, ztrans );

	glColor3f (1,1,1);

	DrawShips ();

	DrawObjects ();

	DrawAmmo ();
	

	glLoadIdentity ();	// reset gl funcs

	DrawHud ();

	glColor3f (1,1,1);

    	/* Draw it to the screen */
    	SDL_GL_SwapBuffers ();

	Update ();
}

void CalculateFPS()
{
	/* SDL_GetTicks() vraci cas v sekundach od inicializace SDL */
	Uint32 miliseconds = SDL_GetTicks () - g_last_time;

	if(miliseconds < 10) {	/* Max ~100 fps */
		SDL_Delay (10-miliseconds); 			/* Cas pro dalsi procesy */
		miliseconds = SDL_GetTicks() - g_last_time;
	}

	g_fps = 1.0f / (miliseconds / 1000.0f);
	g_last_time = SDL_GetTicks ();
}


int Kolize (GLfloat x1, GLfloat z1, GLfloat x2, GLfloat z2)
{
	GLfloat xtrans = me->xpos;
	GLfloat ztrans = me->zpos;
	GLfloat tolerance = 0.1;
	GLuint kolizi = 0;
	
	if ((xtrans <= x1+tolerance && xtrans >= x2-tolerance) || (xtrans >= x1-tolerance && xtrans <= x2+tolerance))
		kolizi++;

	if ((ztrans >= z1-tolerance && ztrans <= z2+tolerance) || (ztrans <= z1+tolerance && ztrans >= z2-tolerance))
		kolizi++;
	
	if (kolizi > 1)
		return 1;

	return 0;
}

void Update ()
{
	SDL_PumpEvents();

	Uint8 *keys = SDL_GetKeyState (NULL);

	if (keys[SDLK_UP] == SDL_PRESSED || keys[SDLK_w] == SDL_PRESSED) {
		/* Move On The X-Plane Based On Player Direction */
		me->speed += 0.001f;

		/* Causes the player to bounce */
		//walkbias = ( float )sin( walkbiasangle * piover180 ) / 20.0f;

		me->state |= SHIP_STATE_UP;
	} else {
		me->state &= ~SHIP_STATE_UP;

		if (me->speed >= 0.0001f)
			me->speed -= 0.0001f;
	}

	if (keys[SDLK_DOWN] == SDL_PRESSED || keys[SDLK_s] == SDL_PRESSED) {
		me->speed -= 0.003f;

		me->state |= SHIP_STATE_DOWN;
	} else
		me->state &= ~SHIP_STATE_DOWN;
	
	/* zmena pohledu hrace */
	if (keys[SDLK_HOME] == SDL_PRESSED)
		view = 1;

	if (keys[SDLK_END] == SDL_PRESSED)
		view = 2;

	if (keys[SDLK_ESCAPE] == SDL_PRESSED) {
		proto_quit ();
		/* ESC key was pressed */
		Quit (0);
	}

	if (keys[SDLK_F1] == SDL_PRESSED) {
		/* F1 key was pressed
		* this toggles fullscreen mode
		*/
		SDL_WM_ToggleFullScreen (surface);
	}

	/* Mys */
	int x, y;

	int flags = SDL_GetMouseState (&x, &y);

	/* Action button was pressed */
	if(flags & SDL_BUTTON (SDL_BUTTON_LEFT)) {
		FireAmmo (me, AMMO_TYPE_EMP);
		me->state |= SHIP_STATE_PRIMARY_WEAPON;
	} else
		me->state &= ~SHIP_STATE_PRIMARY_WEAPON;

	/* Right button was pressed */
	if(flags & SDL_BUTTON (SDL_BUTTON_RIGHT)) {
		FireAmmo (me, AMMO_TYPE_PLASMA);

		me->state |= SHIP_STATE_SECONDARY_WEAPON;
	} else
		me->state &= ~SHIP_STATE_SECONDARY_WEAPON;


	/*if (me->yrot > 360)
		me->yrot = 1;
	if (me->yrot < 1)
		me->yrot = 360;*/

	/* pohyb lodi ve vsech smerech 3D prostoru */
	me->xpos -= (float) sin (me->yrot * piover180) * (me->speed/g_fps);
	me->zpos -= (float) cos (me->yrot * piover180) * (me->speed/g_fps);
	me->ypos += (float) sin (me->urot * piover180) * (me->speed/g_fps);

	/* udela mys akcnejsi :) */
	me->yrot += (((mouse_y) - (me->yrot))*4) / g_fps;
	me->urot += (((mouse_u) - (me->urot))*4) / g_fps;

	/* vypocet FPS */
	CalculateFPS ();
}

unsigned timer_wait;
Uint32 ShipTimer (Uint32 interval, void *param)
{
	allowfire = true;

	ship_ammo_t *ammo;

	for (ammo = ship_ammo_list.next; ammo != &ship_ammo_list; ammo = ammo->next) {
		if (!ammo)
			break;

		if (ammo->lifetime)
			ammo->lifetime --;
	}

	if (me->energy < 1024)
		me->energy ++;

	/* 750ms */
	if (timer_wait > 2) {
		timer_wait = 0;

		if (me->energy > 0)
			me->energy -= (unsigned short) me->speed / 2;
	}

	timer_wait ++;

	return interval;  
}

void MouseHandler (float x, float y)
{
	mouse_y += (float) ((float) -x / (float) config.mouse_speed);	// otaceni doprava, doleva
	mouse_u += (float) ((float) y / (float) config.mouse_speed);	// otaceni nahoru, dolu

	if (mouse_u > 60 || mouse_u < -50)
		mouse_u -= (float) ((float) y / (float) config.mouse_speed);
}
