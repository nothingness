/*
 *  Nothingness - massive multiplayer space action
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "network.h"
#include "config.h"
#include "proto.h"
#include "ship.h"

extern CONFIG config;
extern ship_t *me;
extern bool connected;

bool logged = false;

typedef struct {
	float x;
	float y;
	float z;
} proto_update_t;

typedef struct {
	float speed;
	float yrot;
	float urot;
} proto_turn_t;


int proto_getship (unsigned short id)
{
	printf ("proto_getship\n");

	char str[4];

	str[0] = PROTO_GETSHIP;
	memcpy (str+1, &id, 2);

	return send_to_server (str, 3);
}

int proto_newship (char *buffer, unsigned len)
{
	unsigned short id;

	memcpy (&id, buffer, 2);

	ship_t *ship = ship_add (id);

	if (!ship)
		return 0;

	memcpy (&ship->xpos, buffer+2, 4);
	memcpy (&ship->ypos, buffer+6, 4);
	memcpy (&ship->zpos, buffer+10, 4);
	memcpy (&ship->speed, buffer+14, 4);
	memcpy (&ship->yrot, buffer+18, 4);
	memcpy (&ship->urot, buffer+22, 4);

	memcpy (&ship->cockpit.type, buffer+26, 1);
	memcpy (&ship->engine.type, buffer+27, 1);
	memcpy (&ship->addon.type, buffer+28, 1);
	memcpy (&ship->shield.type, buffer+29, 1);
	memcpy (&ship->pgun.type, buffer+30, 1);
	memcpy (&ship->sgun.type, buffer+31, 1);

	memcpy (&ship->info.name_len, buffer+32, 1);
	
	ship->info.name = (char *) malloc (sizeof (char) * (ship->info.name_len + 1));

	if (!ship->info.name)
		return 0;

	memcpy (ship->info.name, buffer+33, ship->info.name_len);
	ship->info.name[ship->info.name_len] = '\0';

	return 1;
}

int proto_update ()
{
	unsigned l = sizeof (proto_update_t);

	char *str = (char *) malloc (2 + l);

	if (!str)
		return 0;

	proto_update_t update;
	update.x = me->xpos;
	update.y = me->ypos;
	update.z = me->zpos;

	str[0] = PROTO_UPDATE;
	memcpy (str+1, &update, l);

	int ret = send_to_server (str, 1+l);

	free (str);

	return ret;
}

int proto_updateship (char *buffer, unsigned len)
{
	unsigned l = sizeof (proto_update_t) + 2;

	if (!l)
		return 0;

	unsigned short id;

	memcpy (&id, buffer, 2);

	ship_t *ship = ship_find (id);

	if (!ship)
		return proto_getship (id);

	printf ("proto_updateship (%d)\n", id);

	memcpy (&ship->xpos, buffer+2, 4);
	memcpy (&ship->ypos, buffer+6, 4);
	memcpy (&ship->zpos, buffer+10, 4);

	return 1;
}

unsigned char laststate;
unsigned lastyrot;
unsigned lasturot;
unsigned lastspeed;
int proto_turn ()
{
	if (me->state == laststate && (unsigned) me->yrot == lastyrot && (unsigned) me->urot == lasturot && (unsigned) (me->speed * 100) == lastspeed)
		return 0;

	unsigned l = sizeof (proto_turn_t);

	char *str = (char *) malloc (2 + l);

	if (!str)
		return 0;

	proto_turn_t turn;
	turn.speed = me->speed;
	turn.yrot = me->yrot;
	turn.urot = me->urot;

	str[0] = PROTO_TURN;
	memcpy (str+1, &turn, l);

	int ret = send_to_server (str, 1+l);

	free (str);

	laststate = me->state;
	lastyrot = (unsigned) me->yrot;
	lasturot = (unsigned) me->urot;
	lastspeed = (unsigned) (me->speed * 100);

	return ret;
}

int proto_turnship (char *buffer, unsigned len)
{
	unsigned l = sizeof (proto_turn_t) + 2;

	if (!l)
		return 0;

	unsigned short id;

	memcpy (&id, buffer, 2);

	ship_t *ship = ship_find (id);

	if (!ship)
		return proto_getship (id);

	printf ("proto_turnship (%d)\n", id);

	memcpy (&ship->speed, buffer+2, 4);
	memcpy (&ship->yrot, buffer+6, 4);
	memcpy (&ship->urot, buffer+10, 4);

	return 1;
}

int proto_settings (char *buffer, unsigned len)
{
	printf ("proto_settings ()\n");

	return 1;
}

int proto_login ()
{
	printf ("proto_login ()\n");

	/* first create udp session active */
	int ret = send_to_server ("c", 1);

	if (ret <= 0)
		return 0;

	unsigned char ac_len = strlen (config.account);
	unsigned char pwd_len = strlen (config.password);

	char *str = (char *) malloc (sizeof (char) * ac_len + pwd_len + 4);

	if (!str)
		return 0;

	str[0] = PROTO_LOGIN;
	memcpy (str+1, &ac_len, 1);
	memcpy (str+2, config.account, ac_len);
	memcpy (str+2+ac_len, &pwd_len, 1);
	memcpy (str+3+ac_len, config.password, pwd_len);

	ret = send_to_server (str, 3+ac_len+pwd_len);

	free (str);

	return ret;
}

int proto_logged (char *buffer, unsigned len)
{
	printf ("proto_logged ()\n");

	unsigned short id;

	memcpy (&id, buffer, 2);

	ship_t *ship = me;

	if (!ship)
		return 0;

	ship->id = id;

	printf ("INFO -> you are assigned as id %d\n", id);

	memcpy (&ship->xpos, buffer+2, 4);
	memcpy (&ship->ypos, buffer+6, 4);
	memcpy (&ship->zpos, buffer+10, 4);
	memcpy (&ship->speed, buffer+14, 4);
	memcpy (&ship->yrot, buffer+18, 4);
	memcpy (&ship->urot, buffer+22, 4);

	memcpy (&ship->cockpit.type, buffer+26, 1);
	memcpy (&ship->engine.type, buffer+27, 1);
	memcpy (&ship->addon.type, buffer+28, 1);
	memcpy (&ship->shield.type, buffer+29, 1);
	memcpy (&ship->pgun.type, buffer+30, 1);
	memcpy (&ship->sgun.type, buffer+31, 1);

	memcpy (&ship->info.name_len, buffer+32, 1);

	ship->info.name = (char *) malloc (sizeof (char) * (ship->info.name_len + 1));

	if (!ship->info.name)
		return 0;

	memcpy (ship->info.name, buffer+33, ship->info.name_len);
	ship->info.name[ship->info.name_len] = '\0';

	logged = true;

	return 1;
}

int proto_quit ()
{
	if (!connected || !logged)
		return 0;

	printf ("proto_quit ()\n");

	char str[2];

	str[0] = PROTO_QUIT;

	int ret = send_to_server (str, 1);

	logged = false;

	return ret;
}

int proto_shipquit (char *buffer, unsigned len)
{
	if (len != sizeof (unsigned short))
		return 0;

	unsigned short id;

	memcpy (&id, buffer, 2);

	ship_t *ship = ship_find (id);

	if (!ship)
		return 0;

	printf ("proto_shipquit (%d)\n", id);

	/* podle zmenene promenne pozname, ze je nutno vymazat context ze seznamu */
	ship->id = 0;

	return 1;
}

int proto_fire (ship_ammo_t *ammo)
{
	printf ("proto_fire ()\n");

	char str[5];

	str[0] = PROTO_FIRE;
	memcpy (str+1, &ammo->type, 1);

	return send_to_server (str, 2);
}

int proto_shipfire (char *buffer, unsigned len)
{
	if (len != 3)
		return 0;

	unsigned short id;

	memcpy (&id, buffer, 2);

	ship_t *ship = ship_find (id);

	if (!ship)
		return 0;

	printf ("proto_shipfire ()\n");

	unsigned char type = 0;

	memcpy (buffer+2, &type, 1);

	FireAmmo (ship, type);

	return 1;
}

/** PROTOCOL handler */
int proto_handler (char *buffer, unsigned len)
{
	/* pokud jeste nejsme prihlaseni, pak musime cekat na potvrzeni ze serveru */
	if (!logged) {
		if (buffer[0] == PROTO_LOGIN)
			return proto_logged (buffer+1, len-1);

		return 0;
	}

	switch (buffer[0]) {
		case PROTO_TURN:
			return proto_turnship (buffer+1, len-1);
		case PROTO_UPDATE:
			return proto_updateship (buffer+1, len-1);
		case PROTO_FIRE:
			return proto_shipfire (buffer+1, len-1);
		case PROTO_NEWSHIP:
			return proto_newship (buffer+1, len-1);
		case PROTO_SETTINGS:
			return proto_settings (buffer+1, len-1);
		case PROTO_LOGIN:
			return proto_logged (buffer+1, len-1);
		case PROTO_QUIT:
			return proto_shipquit (buffer+1, len-1);
	}

	return 1;
}

int init_proto ()
{
	logged = false;

	return 1;
}


 
