/*
 *  Nothingness - massive multiplayer space action
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "network.h"
#include "proto.h"
#include "config.h"
#include "ship.h"

SDL_mutex *mutex_newship;

extern CONFIG config;
extern ship_t ship_list; // structure of all ships

SDL_TimerID g_timer_id = NULL;
SDL_TimerID g_timer_id2 = NULL;

#define BUFSIZE     1000

bool connected = false;

int fd;

int send_to_server (char *buf, unsigned len)
{
	if (send (fd, buf, len, 0) < 0) {
		puts ("Client -> ERROR -> send_to_server () failed -- server not respond !");
		return 0;
	}

	return 1;
}

Uint32 ship_update (Uint32 interval, void *param)
{
	proto_update ();

	return interval;
}

Uint32 ship_turn (Uint32 interval, void *param)
{
	proto_turn ();

	return interval;
}

int server_session (void *)
{
	char buffer [BUFSIZE+1];

	int ret;

	mutex_newship = SDL_CreateMutex ();

	/* login on server first */
	proto_login ();

	for ( ; ; ) {
		if((ret = recv (fd, buffer, BUFSIZE, 0)) < 0)
			return -1;

		/* handle received data from server */
		if (ret > 0)
			proto_handler (buffer, ret);

		SDL_Delay (10);
	}

	return 0;
}

int connect_toserver ()
{
	if (connected) {
		puts ("Client -> ERROR -> You are already connected !");
		return 0;
	}

	hostent *host;
	sockaddr_in serverSock;

	if ((host = gethostbyname (config.address)) == NULL) {
		printf ("ERROR -> wrong address: %s\n", config.address);
		return -1;
	}

	if ((fd = socket (AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
		printf ("ERROR -> socket () failed\n");
		return -1;
	}

	serverSock.sin_family = AF_INET;
	serverSock.sin_port = htons (config.port);
	memcpy (&(serverSock.sin_addr), host->h_addr, host->h_length);
	
	if (connect (fd, (sockaddr *) &serverSock, sizeof (serverSock)) == -1) {
		printf ("ERROR -> server %s:%d not respond\n", config.address, config.port);
		return -1;
	}

	printf ("INFO -> connected to %s:%d world server\n", config.address, config.port);

	connected = true;

	g_timer_id = SDL_AddTimer (1000, ship_update, NULL);
	g_timer_id2 = SDL_AddTimer (75, ship_turn, NULL);

	SDL_CreateThread (server_session, NULL);

	return 1;
}
