/*
 *  Nothingness - massive multiplayer space action
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _CACHE_H
#define _CACHE_H

#include "textures.h"
#include "3ds/3dsloader.h"

typedef struct cache_tex_context {
	struct cache_tex_context *next, *prev;

	char *name;
	GLuint id;
} cache_tex_t;

typedef struct cache_mdl_context {
	struct cache_mdl_context *next, *prev;

	char *name;
	model3ds_t *model;
} cache_mdl_t;

extern model3ds_t *loadmodel (char *name);
extern GLuint loadtexture (char *name);
extern int init_cache ();

#endif
