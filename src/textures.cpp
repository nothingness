/*
 *  Nothingness - massive multiplayer space action
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "textures.h"
#include <GL/glu.h>

// Prohodi radky v obrazku (prvni za posledni, druhy za predposledni atd.)
bool SwapSurfaceRows (SDL_Surface *surface)
{
	// Ukazatele na prohazovane radky
	Uint8 *hi_row, *lo_row, *tmp_row;

	// Alokace pameti pro pomocny radek
	tmp_row = (Uint8 *)malloc(surface->pitch);

	if(tmp_row == NULL) {
		fprintf(stderr, "Unable to allocate memory.\n");
		return false;
	}

	// Ukazatele na prvni a posledni radek
	hi_row = (Uint8 *)surface->pixels;
	lo_row = hi_row + (surface->h * surface->pitch) - surface->pitch;

	if(SDL_MUSTLOCK(surface)) {
		if(SDL_LockSurface(surface) == -1)
		{
			fprintf(stderr, "Unable to lock surface.\n");
			free(tmp_row);
			return false;
		}
	}

	for(int i = 0; i < surface->h / 2; i++) {
		memcpy(tmp_row, hi_row, surface->pitch);
		memcpy(hi_row, lo_row, surface->pitch);
		memcpy(lo_row, tmp_row, surface->pitch);

		hi_row += surface->pitch;// Dalsi/predchozi radek
		lo_row -= surface->pitch;
	}

	if (SDL_MUSTLOCK(surface)) {
		SDL_UnlockSurface(surface);
	}

	free(tmp_row);	// Uvolni pomocnou pamet

	return true;
}

GLuint CreateTexture (SDL_Surface *surface)
{
	GLuint texture;		// Vysledna textura
	Uint32 saved_flags;	// Pomocne pro ulozeni flagu
	Uint8  saved_alpha;
	SDL_Surface *tmp_img;	// Obrazek pro vytvoreni textury

	// Vytvori prazdny RGB surface
	tmp_img = SDL_CreateRGBSurface(
			SDL_SWSURFACE,		// Softwarovy surface
			surface->w, surface->h,	// Sirka, vyska
			32,			// Barevna hloubka
#if SDL_BYTEORDER == SDL_LIL_ENDIAN
			0x000000FF,		// OpenGL RGBA maska
			0x0000FF00,
			0x00FF0000,
			0xFF000000
#else
			0xFF000000,
			0x00FF0000,
			0x0000FF00,
			0x000000FF
#endif
			);

	if(tmp_img == NULL)
		return 0;

	// Ulozi atributy alfa blendingu

	saved_flags = surface->flags & (SDL_SRCALPHA|SDL_RLEACCELOK);
	saved_alpha = surface->format->alpha;

	//if((saved_flags & SDL_SRCALPHA) == SDL_SRCALPHA)
	//	SDL_SetAlpha(surface, 0, 0);

	// Zkopiruje surface do obrazku pro texturu
	SDL_Rect area;
	area.x = 0;
	area.y = 0;
	area.w = surface->w;
	area.h = surface->h;
	SDL_BlitSurface(surface, &area, tmp_img, &area);

	// Obnovi atributy alfa blendingu
	if((saved_flags & SDL_SRCALPHA) == SDL_SRCALPHA)
		SDL_SetAlpha(surface, saved_flags, saved_alpha);

	// Prohodi radky
	if(!SwapSurfaceRows(tmp_img))
	{
		SDL_FreeSurface(tmp_img);
		return 0;
	}

	// Vytvori texturu
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D,
		     0,
		     GL_RGBA,
		     surface->w, surface->h,
		     0,
		     GL_RGBA,
		     GL_UNSIGNED_BYTE,
		     tmp_img->pixels);

	SDL_FreeSurface(tmp_img);

	return texture;
}

/*GLuint LoadImage(const char* filename)
{
	GLuint texture;
	SDL_Surface *img;

	img = IMG_Load(filename);// Nazapomenout prilinkovat SDL_image

	if(img == NULL)
	{
		fprintf(stderr, "Unable to load '%s': %s\n",
				filename, SDL_GetError());
		return 0;
	}

	texture = CreateTexture(img);
	SDL_FreeSurface(img);

	return texture;
} */

GLuint LoadImage (const char *filename)
{
	GLuint texture = 0;

	SDL_Surface *TextureImage[1];
	
	/* Load The Bitmap, Check For Errors, If Bitmap's Not Found Quit */
	if ((TextureImage[0] = SDL_LoadBMP (filename))) {
		/* Create The Texture */
		glGenTextures( 1, &texture );
	
		/* Load in texture 3 */
		/* Typical Texture Generation Using Data From The Bitmap */
		glBindTexture( GL_TEXTURE_2D, texture );
	
		/* Mipmapped Filtering */
		glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
				GL_LINEAR_MIPMAP_NEAREST );
		glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
				GL_LINEAR );
	
		/* Generate The MipMapped Texture ( NEW ) */
		gluBuild2DMipmaps (GL_TEXTURE_2D, 3, TextureImage[0]->w,
				TextureImage[0]->h, GL_BGR,
				GL_UNSIGNED_BYTE, TextureImage[0]->pixels );
	} else
		printf ("ERROR -> image file %s not found !\n", filename);
	
	/* Free up any memory we may have used */
	if (TextureImage[0])
		SDL_FreeSurface (TextureImage[0]);

	return texture;
}
