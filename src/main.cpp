/*
 *  Nothingness - massive multiplayer space action
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <SDL.h>
#include <SDL_ttf.h>

#include "textures.h"
#include "audio.h"
#include "scene.h"
#include "network.h"
#include "config.h"
#include "ship.h"
#include "cache.h"

/* screen width, height, and bit depth */
#define SCREEN_WIDTH  1280
#define SCREEN_HEIGHT 1024
#define SCREEN_BPP    32

#define CONFIG_FILENAME	"./data/config/client.conf"

extern SDL_Surface *surface;
CONFIG config;


int config_set (char *buffer, char *c)
{
	if (!strncmp (buffer, "account", 7))
		config.account = strdup (c);

	if (!strncmp (buffer, "password", 8))
		config.password = strdup (c);

	if (!strncmp (buffer, "address", 7))
		config.address = strdup (c);

	if (!strncmp (buffer, "port", 4))
		config.port = atoi (c);

	if (!strncmp (buffer, "mouse_speed", 11))
		config.mouse_speed = atoi (c);
	
	if (!strncmp (buffer, "screen", 6))
		config.screen = strdup (c);

	return 1;
}

int config_load ()
{
	unsigned int l;
	FILE *fp;
	
	char *buffer, *c;
	
	fp = fopen (CONFIG_FILENAME, "r");
	if (!fp) {
		puts ("Config -> " CONFIG_FILENAME " does not exist");
		return 0;
	}

	buffer = (char *) calloc (4097, sizeof (char));

	if (!buffer)
		return 0;

	while (!feof (fp)) {
		fgets (buffer, 4096, fp);
		for (c = buffer; *c && *c != ' '; c++);
		if (!*c)
			continue;

		*c++ = '\0';

		l = strlen (c);
		if (c[l - 1] == '\n')
			c[l-- - 1] = '\0';

		if (buffer[0] == '#')	// nevypisovat komentar
			continue;

		config_set (buffer, c);

		printf ("CONFIG ->'%s' : '%s'\n", buffer, c);
	}
	
	free (buffer);
	fclose (fp);
	
	return 1;
}

/* function to release/destroy our resources and restoring the old desktop */
void Quit (int returnCode)
{
	/* clean up the window */
	SDL_Quit ();

	/* and exit appropriately */
	exit (returnCode);
}

/* function to reset our viewport after a window resize */
int resizeWindow (int width, int height)
{
	/* Height / width ration */
	GLfloat ratio;

	/* Protect against a divide by zero */
	if ( height == 0 )
	    height = 1;

	ratio = ( GLfloat )width / ( GLfloat )height;

	/* Setup our viewport. */
	glViewport( 0, 0, ( GLint )width, ( GLint )height );

	/* change to the projection matrix and set our viewing volume-> */
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity( );

	/* Set our perspective */
	gluPerspective (90.0f, ratio, 0.01f, 8000.0f);

	/* Make sure we're chaning the model view and not the projection */
	glMatrixMode (GL_MODELVIEW);

	/* Reset The View */
	glLoadIdentity ();

	return (TRUE);
}

extern bool logged;
unsigned screen_x, screen_y, screen_bpp;
int main( int argc, char **argv )
{
	if (!config_load ()) // CONFIG
		return 0;

	sscanf (config.screen, "%d %d %d", &screen_x, &screen_y, &screen_bpp);

	init_cache ();

	init_ship ();

/*	unsigned retry = 10;
	while (!logged) {
		SDL_Delay (100);

		if (!retry)
			Quit (1);

		retry --;
	}*/

	/* Flags to pass to SDL_SetVideoMode */
	int videoFlags;
	/* main loop variable */
	int done = FALSE;
	/* used to collect events */
	SDL_Event event;
	/* this holds some info about our display */
	const SDL_VideoInfo *videoInfo;
	/* whether or not the window is active */
	int isActive = TRUE;

	/* initialize SDL */
	if (SDL_Init (SDL_INIT_VIDEO | SDL_INIT_TIMER) < 0) {
		fprintf (stderr, "Video & Timer initialization failed: %s\n", SDL_GetError ());
		Quit (1);
	}

	/* Fetch the video info */
	videoInfo = SDL_GetVideoInfo ();

	if (!videoInfo) {
		fprintf (stderr, "Video query failed: %s\n", SDL_GetError ());
		Quit (1);
	}

	/* the flags to pass to SDL_SetVideoMode */
	videoFlags  = SDL_OPENGL;          /* Enable OpenGL in SDL */
	videoFlags |= SDL_GL_DOUBLEBUFFER; /* Enable double buffering */
	videoFlags |= SDL_HWPALETTE;       /* Store the palette in hardware */
	videoFlags |= SDL_RESIZABLE;       /* Enable window resizing */

	/* This checks to see if surfaces can be stored in memory */
	if (videoInfo->hw_available)
		videoFlags |= SDL_HWSURFACE;
	else
		videoFlags |= SDL_SWSURFACE;

	/* This checks if hardware blits can be done */
	if (videoInfo->blit_hw)
		videoFlags |= SDL_HWACCEL;

	/* Sets up OpenGL double buffering */
	SDL_GL_SetAttribute (SDL_GL_DOUBLEBUFFER, 1);

	/* get a SDL surface */
	surface = SDL_SetVideoMode (screen_x, screen_y, screen_bpp, videoFlags);

	/* Verify there is a surface */
	if (!surface) {
		fprintf (stderr, "Video mode set failed: %s\n", SDL_GetError ());
		Quit (1);
	}

	/* Enable key repeat */
	if ((SDL_EnableKeyRepeat (100, SDL_DEFAULT_REPEAT_INTERVAL))) {
		fprintf (stderr, "Setting keyboard repeat failed: %s\n", SDL_GetError ());
		Quit (1);
	}

	//InitAudio ();

	/* Initialize OpenGL */
	if (!InitGL ())
		return 0;

	if (config.mouse_speed < 1)
		config.mouse_speed = 1;

	SDL_WM_ToggleFullScreen (surface); /* Prejde do fullscreenu */
	SDL_ShowCursor(SDL_DISABLE);

	/* resize the initial window */
	resizeWindow (screen_x, screen_y);

	/* wait for events */
	while (!done) {
		// handle the events in the queue 

		while (SDL_PollEvent (&event)) {
			switch (event.type) {
				case SDL_MOUSEMOTION:
					MouseHandler (event.motion.xrel, event.motion.yrel);

					fflush (stdout);
					break;
				case SDL_ACTIVEEVENT:
					if (event.active.gain == 0)
						isActive = FALSE;
					else
						isActive = TRUE;

					break;
				case SDL_VIDEORESIZE:
					surface = SDL_SetVideoMode (event.resize.w, event.resize.h, 16, videoFlags);
					if (!surface) {
						fprintf (stderr, "Could not get a surface after resize: %s\n", SDL_GetError ());
						Quit(1);
					}

					resizeWindow (event.resize.w, event.resize.h);
					break;
				case SDL_QUIT:
					done = TRUE;
					break;
				default:
					break;
			}
		}

		//if (isActive)
			DrawGLScene();

		SDL_Delay (1);
	}

	/* clean ourselves up and exit */
	Quit (0);

	/* Should never get here */
	return 0;
}
