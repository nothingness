/*
 *  BadEvil - massive multiplayer action
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _FONT_H
#define _FONT_H

	
extern bool InitFont ();
extern GLvoid glPrint ( const char *fmt, ... );
extern GLvoid glPrint3f (float x, float y, float z, float size, char *string, int set);
	//extern void glPrint (GLint x, GLint y, float dim, float r, float g, float b, char *string, int set);

#endif
