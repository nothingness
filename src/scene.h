/*
 *  Nothingness - massive multiplayer space action
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _SCENE_H
#define _SCENE_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "textures.h"
#include "audio.h"
#include "ship.h"

/* Set up some booleans */
#define TRUE  1
#define FALSE 0

#define	SHIP_MODEL_PATH		"./data/models/ship/"
#define	OBJECT_MODEL_PATH	"./data/models/object/"
#define	OBJECT_TEXTURE_PATH	"./data/textures/"
#define	OBJECT_WORLD_PATH	"./data/world/1.list"

/* Object types */
#define OBJECT_TYPE_MODEL	0x0
#define OBJECT_TYPE_SUN		0x1
#define OBJECT_TYPE_ASTEROID	0x2
#define OBJECT_TYPE_STAR	0x3
#define OBJECT_TYPE_PLANET	0x4

#define Contact(X1,Y1,Z1,X2,Y2,Z2) sqrt( (X1-X2)*(X1-X2)+(Y1-Y2)*(Y1-Y2)+(Z1-Z2)*(Z1-Z2) )
#define VectorLength( v ) sqrt(v.x * v.x + v.y * v.y + v.z * v.z)

extern int InitGL (GLvoid);
extern void DrawGLScene ();
extern void Quit (int returnCode);
extern void MouseHandler (float x, float y);
extern unsigned FireAmmo (ship_t *owner, unsigned char type);

#endif
  
