/*
 *  Nothingness - massive multiplayer space action
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <lib3ds/file.h>
#include <lib3ds/mesh.h> 
#include <SDL.h>
#include "3dsloader.h"

static void getfaces (model3ds_t *m3ds)
{	
	Lib3dsFile *m_model = m3ds->m_model;
	m3ds->m_TotalFaces = 0;
	Lib3dsMesh *mesh;
	// Loop through every mesh
	for (mesh = m_model->meshes; mesh != NULL; mesh = mesh->next) {
		// Add the number of faces this mesh has to the total faces
		m3ds->m_TotalFaces += mesh->faces;
	}
}

// Copy vertices and normals to the memory of the GPU
static void createvbo (model3ds_t *m3ds)
{
	Lib3dsFile *m_model = m3ds->m_model;

	// Calculate the number of faces we have in total
	getfaces (m3ds);
	
	// Allocate memory for our vertices and normals
	Lib3dsVector *vertices = new Lib3dsVector[m3ds->m_TotalFaces * 3];
	Lib3dsVector *normals = new Lib3dsVector[m3ds->m_TotalFaces * 3];
	Lib3dsTexel *texCoords = new Lib3dsTexel[m3ds->m_TotalFaces * 32];
	
	Lib3dsMesh *mesh;
	unsigned int FinishedFaces = 0;
	// Loop through all the meshes
	for (mesh = m_model->meshes;mesh != NULL;mesh = mesh->next) {
		lib3ds_mesh_calculate_normals(mesh, &normals[FinishedFaces*3]);
		// Loop through every face
		for (unsigned int cur_face = 0; cur_face < mesh->faces;cur_face++) {
			Lib3dsFace *face = &mesh->faceL[cur_face];

			for(unsigned int i = 0;i < 3;i++) {
				memcpy(&vertices[FinishedFaces*3 + i], mesh->pointL[face->points[i]].pos, sizeof(Lib3dsVector));

				if(mesh->texels)    // texture coordinates
					memcpy(&texCoords[FinishedFaces*3+i], mesh->texelL[face->points[ i ]], sizeof(Lib3dsTexel));
			}

			FinishedFaces++;
		}
	}

	// Generate a Vertex Buffer Object and store it with our vertices
	glGenBuffers(1, &m3ds->m_VertexVBO);
	glBindBuffer(GL_ARRAY_BUFFER,m3ds->m_VertexVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Lib3dsVector) * 3 * m3ds->m_TotalFaces, vertices, GL_STATIC_DRAW);
	
	// Generate another Vertex Buffer Object and store the normals in it
	glGenBuffers(1, &m3ds->m_NormalVBO);
	glBindBuffer(GL_ARRAY_BUFFER, m3ds->m_NormalVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Lib3dsVector) * 3 * m3ds->m_TotalFaces, normals, GL_STATIC_DRAW);

	// Generate another Vertex Buffer Object and store the normals in it
	glGenBuffers(1, &m3ds->m_TexVBO);
	glBindBuffer(GL_ARRAY_BUFFER, m3ds->m_TexVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Lib3dsVector) * 2 * m3ds->m_TotalFaces, texCoords, GL_STATIC_DRAW);

	// Clean up our allocated memory
	delete vertices;
	delete normals;
	delete texCoords;

	// We no longer need lib3ds
	lib3ds_file_free (m_model);
	m_model = NULL;
}

model3ds_t *load3ds (char *filename)
{
	Lib3dsFile *m_model = lib3ds_file_load (filename);

	if (!m_model) {
		printf ("ERROR -> 3dsload () failed - file %s\n", filename);
		return 0;
	}

	model3ds_t *m3ds = (model3ds_t *) malloc (sizeof (model3ds_t));

	if (!m3ds)
		return 0;
	
	m3ds->m_model = m_model;

	createvbo (m3ds);

	return m3ds;
}

void draw3ds (model3ds_t *m3ds)
{
	glPushMatrix ();

	// Enable vertex and normal arrays
	glEnableClientState (GL_VERTEX_ARRAY);
	glEnableClientState (GL_NORMAL_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);// Zapne texture coord arrays
	
	// Bind the vbo with the normals
	glBindBuffer (GL_ARRAY_BUFFER, m3ds->m_NormalVBO);
	// The pointer for the normals is NULL which means that OpenGL will use the currently bound vbo
	glNormalPointer (GL_FLOAT, 0, NULL);
	
	glBindBuffer (GL_ARRAY_BUFFER, m3ds->m_VertexVBO);
	glVertexPointer (3, GL_FLOAT, 0, NULL);

	glBindBufferARB(GL_ARRAY_BUFFER_ARB, m3ds->m_TexVBO);
	glTexCoordPointer (2, GL_FLOAT, 0, (char *) NULL);

	glBindTexture (GL_TEXTURE_2D, m3ds->texture); // We set the active texture

	// Render the triangles
	glDrawArrays (GL_TRIANGLES, 0, m3ds->m_TotalFaces * 3);
	
	glDisableClientState (GL_VERTEX_ARRAY);
	glDisableClientState (GL_NORMAL_ARRAY);

	glPopMatrix ();
}
