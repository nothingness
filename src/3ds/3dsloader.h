/*
 *  Nothingness - massive multiplayer space action
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _3DSLOADER_H
#define _3DSLOADER_H

#define GL_GLEXT_PROTOTYPES

#include <lib3ds/file.h>
#include <lib3ds/mesh.h> 
#include <GL/gl.h>
#include <GL/glu.h>
#include <SDL.h>

typedef struct {
	unsigned int m_TotalFaces;
	Lib3dsFile *m_model;
	GLuint m_VertexVBO, m_NormalVBO, m_TexVBO;
	GLuint texture;
} model3ds_t;

extern model3ds_t *load3ds (char *filename);
extern void draw3ds (model3ds_t *m3ds);

#endif
