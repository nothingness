/*
 *  Nothingness - massive multiplayer space action
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _SHIP_H
#define _SHIP_H

#include "3ds/3dsloader.h"

/* stihaci */
#define SHIP_TYPE_DARKWOLF			0x1
#define SHIP_TYPE_VELOCITY			0x2
#define SHIP_TYPE_RAPTOR			0x3
#define SHIP_TYPE_HERMES			0x4
#define SHIP_TYPE_ESPERANTO			0x5

/* prepravni */
#define SHIP_TYPE_COLLOSSUM			0x6

#define ENGINE_TYPE_RULLER			0x1
#define ENGINE_TYPE_GT				0x2
#define ENGINE_TYPE_EXPOWER			0x3
#define ENGINE_TYPE_ROXOR			0x4

#define WEAPON_TYPE_MACHINEGUN			0x1
#define WEAPON_TYPE_PLASMAGUN			0x2
#define WEAPON_TYPE_EMGUN			0x3
#define WEAPON_TYPE_LASERGUN			0x4

#define SHIP_STATE_UP				0x1
#define SHIP_STATE_DOWN				0x2
#define SHIP_STATE_PRIMARY_WEAPON		0x4
#define SHIP_STATE_SECONDARY_WEAPON		0x8
#define SHIP_STATE_SHIELD			0x10
#define SHIP_STATE_ENGINE			0x20
#define SHIP_STATE_HYPERDRIVE			0x40
#define SHIP_STATE_ENERGY			0x80

#define AMMO_TYPE_BULLET			0x1
#define AMMO_TYPE_PLASMA			0x2
#define AMMO_TYPE_EMP				0x3
#define AMMO_TYPE_LASER				0x4

typedef struct {
	char *name;
	unsigned char name_len;
} ship_info_t;

typedef struct {
	unsigned char type;
	unsigned char damage;
	model3ds_t *model;
} ship_cockpit_t;

typedef struct {
	unsigned char type;
	unsigned char damage;
	model3ds_t *model;
} ship_engine_t;

typedef struct {
	unsigned char type;
	unsigned char damage;
} ship_addon_t;

typedef struct {
	unsigned char type;
	unsigned char damage;
} ship_shield_t;

typedef struct {
	unsigned char type;
	unsigned char damage;
} ship_pgun_t;

typedef struct {
	unsigned char type;
	unsigned char damage;
} ship_sgun_t;

/* Ship structure */
typedef struct ship_context {
	struct ship_context *next, *prev;

	float xpos;
	float ypos;
	float zpos;
	float yrot;
	float urot;
	float speed;

	short energy;

	unsigned short id;

	unsigned char state;

	ship_info_t info;

	ship_cockpit_t cockpit;
	ship_engine_t engine;
	ship_addon_t addon;
	ship_shield_t shield;
	ship_pgun_t pgun;
	ship_sgun_t sgun;
} ship_t;

typedef struct ship_ammo_context {
	struct ship_ammo_context *next, *prev;

	ship_t *owner;

	unsigned char type;
	unsigned char lifetime;

	float speed;
	float xpos;
	float ypos;
	float zpos;
	float yrot;
	float urot;
} ship_ammo_t;

extern ship_t *me;	// my ship
extern int init_ship ();
extern ship_t *ship_add (unsigned short id);
extern bool ship_del (ship_t *ship);
extern ship_t *ship_find (unsigned short id);
extern ship_ammo_t *ship_ammo_register (ship_t *ship);
extern bool ship_ammo_delete (ship_ammo_t *ammo);

#endif
