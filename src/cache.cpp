/*
 *  Nothingness - massive multiplayer space action
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "cache.h"


cache_tex_t texture_list;
cache_mdl_t model_list;


cache_mdl_t *cache_model_find (char *name)
{
	cache_mdl_t *cache;
	for (cache = model_list.next; cache != &model_list; cache = cache->next)
		if (!strcmp (cache->name, name))
			return cache;

	return 0;
}

cache_tex_t *cache_texture_find (char *name)
{
	cache_tex_t *cache;
	for (cache = texture_list.next; cache != &texture_list; cache = cache->next)
		if (!strcmp (cache->name, name))
			return cache;

	return 0;
}


cache_mdl_t *cache_model_add (char *name)
{
	// alloc and init context
	cache_mdl_t *cache = (cache_mdl_t *) malloc (sizeof (cache_mdl_t));

	if (!cache)
		return 0;

	cache->model = load3ds (name);

	if (!cache->model) {
		free (cache);
		return 0;
	}

	cache->name = strdup (name);

	// add into list
	cache->next = &model_list;
	cache->prev = model_list.prev;
	cache->prev->next = cache;
	cache->next->prev = cache;
	
	return cache;
}

cache_tex_t *cache_texture_add (char *name)
{
	// alloc and init context
	cache_tex_t *cache = (cache_tex_t *) malloc (sizeof (cache_tex_t));

	if (!cache)
		return 0;

	cache->name = strdup (name);

	cache->id = LoadImage (cache->name);

	// add into list
	cache->next = &texture_list;
	cache->prev = texture_list.prev;
	cache->prev->next = cache;
	cache->next->prev = cache;

	return cache;
}

/*
bool cache_del (ship_t *ship)
{
	printf ("CLIENT -> Ship '%s' was removed from list\n", ship->info.name);

 	ship->next->prev = ship->prev;
 	ship->prev->next = ship->next;
	
	return 1;
}
*/

model3ds_t *loadmodel (char *name)
{
	cache_mdl_t *cache = cache_model_find (name);

	if (!cache) {
		cache = cache_model_add (name);

		if (!cache)
			return 0;
	}

	return cache->model;
}

GLuint loadtexture (char *name)
{
	printf ("loadtexture (%s)\n", name);

	cache_tex_t *cache = cache_texture_find (name);

	if (!cache) {
		cache = cache_texture_add (name);

		if (!cache)
			return 0;
	}

	return cache->id;
}

int init_cache ()
{
	model_list.next = &model_list;
	model_list.prev = &model_list;

	texture_list.next = &texture_list;
	texture_list.prev = &texture_list;

	return 1;
}
