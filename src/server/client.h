/*
 *  Nothingness - massive multiplayer space action
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _CLIENT_H
#define _CLIENT_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "network.h"
#include "ship.h"

/* Client structure */
typedef struct client_context {
	struct client_context *next, *prev;

	struct sockaddr_in sockaddr;
	socklen_t socklen;

	void *ship;
	unsigned short id;
} client_t;

extern client_t *client_register (struct sockaddr_in *sockaddr);
extern client_t *client_find (struct sockaddr_in *sockaddr);
extern client_t *client_findbyid (unsigned short id);
extern int client_logout (client_t *client);
extern int init_client ();

#endif

