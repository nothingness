/*
 *  Nothingness - massive multiplayer space action server
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "account.h"

account_t account_list;


account_t *account_find (char *account, unsigned char len)
{
	account_t *a;

	for (a = account_list.next; a != &account_list; a = a->next) {
		if (!strncmp (a->account, account, len))
 			return a;
	}

	return 0;
}

account_t *account_register (char *account, unsigned char ac_len, char *password, unsigned char pwd_len)
{
	/* alloc and init context */
	account_t *a = (account_t *) malloc (sizeof (account_t));

	if (!a)
		return 0;

	a->ac_len = ac_len;
	a->account = (char *) malloc (sizeof (char) * (ac_len + 1));

	if (!a->account) {
		free (a);
		return 0;
	}

	memcpy (a->account, account, ac_len);
	a->account[ac_len] = '\0';

	a->pwd_len = pwd_len;
	a->password = (char *) malloc (sizeof (char) * (pwd_len + 1));

	if (!a->password) {
		free (a->account);
		free (a);
		return 0;
	}

	memcpy (a->password, password, pwd_len);
	a->password[pwd_len] = '\0';

	a->logged = 0;

	/* add into list */
	a->next = &account_list;
	a->prev = account_list.prev;
	a->prev->next = a;
	a->next->prev = a;

	return a;
}

account_t *account_login (char *account, unsigned char ac_len, char *password, unsigned char pwd_len)
{
	/* alloc and init context */
	account_t *a = account_find (account, ac_len);

	if (!a)
		return 0;

	if (a->logged)
		return 0;

	if (strncmp (a->password, password, pwd_len))
		return 0;

	a->logged = 1;

	return a;
}

int account_delete (account_t *account)
{
	account->next->prev = account->prev;
	account->prev->next = account->next;

	account->logged = 0;

	free (account->account);
	free (account->password);
	free (account);

	return 1;
}

int init_account ()
{
	account_list.next = &account_list;
	account_list.prev = &account_list;

	account_register ("admin", 5, "root", 4);
	account_register ("test", 4, "root", 4);

	return 1;
}


 
