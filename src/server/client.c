/*
 *  Nothingness - massive multiplayer space action server
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "client.h"

unsigned short curr_id;
client_t client_list;

client_t *client_register (struct sockaddr_in *sockaddr)
{
	if (!sockaddr->sin_addr.s_addr)
		return 0;

	/* alloc and init context */
	client_t *client = (client_t *) malloc (sizeof (client_t));

	if (!client)
		return 0;

	client->socklen = sizeof (struct sockaddr_in);
	memcpy (&client->sockaddr, sockaddr, client->socklen);

	client->id = curr_id ++;

	client->ship = 0;

	/* add into list */
	client->next = &client_list;
	client->prev = client_list.prev;
	client->prev->next = client;
	client->next->prev = client;

	return client;
}

int client_logout (client_t *client)
{
	client->next->prev = client->prev;
	client->prev->next = client->next;

	free (client);

	return 1;
}

client_t *client_find (struct sockaddr_in *sockaddr)
{
	client_t *client;

	for (client = client_list.next; client != &client_list; client = client->next) {
		if (client->sockaddr.sin_addr.s_addr == sockaddr->sin_addr.s_addr && client->sockaddr.sin_port == sockaddr->sin_port)
 			return client;
	}

	return 0;
}

client_t *client_findbyid (unsigned short id)
{
	client_t *client;

	for (client = client_list.next; client != &client_list; client = client->next) {
		if (client->id == id)
 			return client;
	}

	return 0;
}

int init_client ()
{
	client_list.next = &client_list;
	client_list.prev = &client_list;

	curr_id = 1;

	return 1;
}


 
