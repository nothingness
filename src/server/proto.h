/*
 *  Nothingness - massive multiplayer space action server
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _PROTO_H
#define _PROTO_H

#include <stdio.h>
#include <string.h>
#include "client.h"

#define PROTO_LOGIN	'l'
#define PROTO_SETTINGS	's'
#define PROTO_QUIT	'q'
#define PROTO_UPDATE	'u'
#define PROTO_TURN	't'
#define PROTO_NEWSHIP   'n'
#define PROTO_GETSHIP   'g'
#define PROTO_FIRE	'f'

extern int proto_handler (client_t *client, char *buffer, unsigned len);
extern int init_proto ();

#endif

 
