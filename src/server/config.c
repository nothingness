/*
 *  Nothingness - massive multiplayer space action server
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "config.h"

CONFIG config;

int config_set (char *buffer, char *c)
{
	if (!strncmp (buffer, "port", 4))
		config.port = atoi (c);

	if (!strncmp (buffer, "name", 4))
		config.name = strdup (c);

	if (!strncmp (buffer, "dbuser", 6))
		config.dbuser = strdup (c);

	if (!strncmp (buffer, "dbpass", 6))
		config.dbpass = strdup (c);

	if (!strncmp (buffer, "dbserver", 8))
		config.dbserver = strdup (c);

	if (!strncmp (buffer, "dbname", 6))
		config.dbname = strdup (c);

	return 1;
}

int config_load ()
{
	unsigned int l;
	FILE *fp;
	
	char *buffer, *c;
	
	fp = fopen (CONFIG_FILENAME, "r");
	if (!fp) {
		puts ("Config -> " CONFIG_FILENAME " does not exist");
		return 0;
	}

	buffer = (char *) calloc (4097, sizeof (char));

	if (!buffer)
		return 0;

	while (!feof (fp)) {
		fgets (buffer, 4096, fp);
		for (c = buffer; *c && *c != ' '; c++);
		if (!*c)
			continue;

		*c++ = '\0';

		l = strlen (c);
		if (c[l - 1] == '\n')
			c[l-- - 1] = '\0';

		if (buffer[0] == '#')	// nevypisovat komentar
			continue;

		config_set (buffer, c);

		printf ("CONFIG ->'%s' : '%s'\n", buffer, c);
	}
	
	free (buffer);
	fclose (fp);
	
	return 1;
}

int init_config ()
{
	return config_load ();
}


 
