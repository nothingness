/*
 *  Nothingness - massive multiplayer space action server
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "mysql.h"
#include "config.h"

extern CONFIG config;

sql_t *sql;

sql_t *mysql_connect (char *host, char *user, char *pass, char *db)
{
	sql_t *session = (sql_t *) malloc (sizeof (sql_t));

	if (!session)
		return 0;

        mysql_init (&(session->mysql));

        if (!mysql_real_connect (&(session->mysql), host, user, pass, db, 0, NULL, 0)) {
		printf ("ERROR -> mysql_connect (): %s\n", mysql_error (&(session->mysql)));
		free (session);
		return 0;
	}

	return session;
}

int mysql_command (char *cmd)
{
	if (!sql)
		return 0;

	if (mysql_query (&(sql->mysql), cmd)) {
		printf ("ERROR -> mysql_command (): %s\n", mysql_error (&(sql->mysql)));
		return 0;
        }

	return 1;
}

int mysql_createtables ()
{
	char *sql_cmd[] = 
	{
		"CREATE TABLE IF NOT EXISTS account ("
			"name VARCHAR(32),"
			"pass VARCHAR(32)"
		" )",

		NULL
	};

	int i;
	
	for (i = 0; sql_cmd[i]; i ++) {
		if (!mysql_command (sql_cmd[i]))
			return 0;
	}

	return 0;
}

int init_mysql ()
{
	/* connect to mysql server */
	//sql = mysql_connect (config.dbserver, config.dbuser, config.dbpass, config.dbname);

	//if (!sql)
	//	return 0;

	/* create database tables if not exists */
	//mysql_createtables ();

	return 1;
}


 
