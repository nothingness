/*
 *  Nothingness - massive multiplayer space action server
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _NETWORK_H
#define _NETWORK_H

#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>
#include "client.h"

#define NETWORK_BUFFER_SIZE	2048

typedef struct {
	int fd;
	struct sockaddr_in sockaddr;
	struct sockaddr_in client;
	socklen_t clientlen;
	char *buffer;
} NETWORK;

extern int send_to_client (void *client, char *buffer, unsigned len);
extern int send_to_all (char *buffer, unsigned len);
extern int send_to_allex (void *client, char *buffer, unsigned len);
extern int network_handler ();
extern int init_network ();

#endif

 
