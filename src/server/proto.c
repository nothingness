/*
 *  Nothingness - massive multiplayer space action server
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "network.h"
#include "account.h"
#include "client.h"
#include "proto.h"
#include "ship.h"

typedef struct {
	float x;
	float y;
	float z;
} proto_update_t;

typedef struct {
	float speed;
	float yrot;
	float urot;
} proto_turn_t;


int proto_getship (client_t *client, char *buffer, unsigned len)
{
	if (len != sizeof (unsigned short))
		return 0;

	unsigned short id = (unsigned short) *buffer;

	printf ("proto_getship () -> '%s' : %d\n", buffer, len);

	ship_t *ship = ship_find (id);

	char str[36+ship->info.name_len];

	str[0] = PROTO_NEWSHIP;
	memcpy (str+1, &id, 2);

	memcpy (str+3, &ship->xpos, 4);
	memcpy (str+7, &ship->ypos, 4);
	memcpy (str+11, &ship->zpos, 4);
	memcpy (str+15, &ship->speed, 4);
	memcpy (str+19, &ship->yrot, 4);
	memcpy (str+23, &ship->urot, 4);

	memcpy (str+27, &ship->cockpit_type, 1);
	memcpy (str+28, &ship->engine_type, 1);
	memcpy (str+29, &ship->addon_type, 1);
	memcpy (str+30, &ship->shield_type, 1);
	memcpy (str+31, &ship->pgun_type, 1);
	memcpy (str+32, &ship->sgun_type, 1);

	memcpy (str+33, &ship->info.name_len, 1);
	memcpy (str+34, ship->info.name, ship->info.name_len);

	return send_to_client (client, str, 34+ship->info.name_len);
}

int proto_login (client_t *client, char *buffer, unsigned len)
{
	printf ("proto_login () -> '%s' : %d\n", buffer, len);

	unsigned char *account_len = (unsigned char *) buffer;
	unsigned char *password_len = (unsigned char *) buffer + *account_len;

	unsigned char ac_len = *account_len;
	unsigned char pwd_len = *password_len;

	account_t *account = account_login (buffer+1, ac_len, buffer+2+ac_len, pwd_len);

	if (!account) {
		printf ("WARNING -> client with id: %d enter wrong password or his account is already used\n", client->id);
		return 0;
	}

	client->ship = ship_login (account);

	if (!client->ship)
		return 0;

	printf ("PROTO -> client with account %s was logged succefully\n", account->account);

	ship_t *ship = client->ship;

	char str[36+ship->info.name_len];
	str[0] = PROTO_LOGIN;
	memcpy (str+1, &client->id, 2);

	memcpy (str+3, &ship->xpos, 4);
	memcpy (str+7, &ship->ypos, 4);
	memcpy (str+11, &ship->zpos, 4);
	memcpy (str+15, &ship->speed, 4);
	memcpy (str+19, &ship->yrot, 4);
	memcpy (str+23, &ship->urot, 4);

	memcpy (str+27, &ship->cockpit_type, 1);
	memcpy (str+28, &ship->engine_type, 1);
	memcpy (str+29, &ship->addon_type, 1);
	memcpy (str+30, &ship->shield_type, 1);
	memcpy (str+31, &ship->pgun_type, 1);
	memcpy (str+32, &ship->sgun_type, 1);

	memcpy (str+33, &ship->info.name_len, 1);
	memcpy (str+34, ship->info.name, ship->info.name_len);

	int ret = send_to_client (client, str, 34+ship->info.name_len);

	str[0] = PROTO_NEWSHIP;

	send_to_allex (client, str, 34+ship->info.name_len);

	return ret;
}

int proto_update (client_t *client, char *buffer, unsigned len)
{
	if (len != sizeof (proto_update_t))
		return 0;

	printf ("proto_update () -> '%s' : %d\n", buffer, len);

	ship_t *ship = client->ship;

	if (!ship)
		return 0;

	if (!ship->account)
		return 0;

	if (!ship->account->logged)
		return 0;

	proto_update_t *update = (proto_update_t *) buffer;

	ship->xpos = update->x;
	ship->ypos = update->y;
	ship->zpos = update->z;

	char str[4+len];
	str[0] = PROTO_UPDATE;
	memcpy (str+1, &client->id, 2);
	memcpy (str+3, buffer, len);

	return send_to_allex (client, str, 3+len);
}

int proto_turn (client_t *client, char *buffer, unsigned len)
{
	if (len != sizeof (proto_turn_t))
		return 0;

	printf ("proto_turn () -> '%s' : %d\n", buffer, len);

	ship_t *ship = client->ship;

	if (!ship)
		return 0;

	if (!ship->account)
		return 0;

	if (!ship->account->logged)
		return 0;

	proto_turn_t *turn = (proto_turn_t *) buffer;

	ship->speed = turn->speed;
	ship->yrot = turn->yrot;
	ship->urot = turn->urot;

	char str[4+len];
	str[0] = PROTO_TURN;
	memcpy (str+1, &client->id, 2);
	memcpy (str+3, buffer, len);

	return send_to_allex (client, str, 3+len);
}

int proto_fire (client_t *client, char *buffer, unsigned len)
{
	if (len != 1)
		return 0;

	printf ("proto_fire () -> '%s' : %d\n", buffer, len);

	ship_t *ship = client->ship;

	if (!ship)
		return 0;

	if (!ship->account)
		return 0;

	if (!ship->account->logged)
		return 0;

	unsigned char type = (unsigned char) *buffer;

	char str[5];
	str[0] = PROTO_FIRE;
	memcpy (str+1, &client->id, 2);
	memcpy (str+3, &type, 1);

	return send_to_allex (client, str, 4);
}

int proto_quit (client_t *client)
{
	printf ("proto_quit ()\n");

	ship_t *ship = client->ship;

	if (!ship)
		return 0;

	if (!ship->account)
		return 0;

	if (!ship->account->logged)
		return 0;

	ship->account->logged = 0; 

	ship_quit (client->ship);

	char str[4];
	str[0] = PROTO_QUIT;
	memcpy (str+1, &client->id, 2);

	client_logout (client);

	return send_to_allex (client, str, 3);
}

/** PROTOCOL handler */
int proto_handler (client_t *client, char *buffer, unsigned len)
{
	switch (buffer[0]) {
		case PROTO_TURN:
			return proto_turn (client, buffer+1, len-1);
		case PROTO_UPDATE:
			return proto_update (client, buffer+1, len-1);
		case PROTO_FIRE:
			return proto_fire (client, buffer+1, len-1);
		case PROTO_GETSHIP:
			return proto_getship (client, buffer+1, len-1);
		case PROTO_LOGIN:
			return proto_login (client, buffer+1, len-1);
		case PROTO_QUIT:
			return proto_quit (client);
	}

	return 1;
}

int init_proto ()
{

	return 1;
}


 
