/*
 *  Nothingness - massive multiplayer space action
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _SHIP_H
#define _SHIP_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "client.h"
#include "account.h"

typedef struct {
	char *name;
	unsigned char name_len;
} ship_info_t;

/* Ship structure */
typedef struct ship_context {
	struct ship_context *next, *prev;

	account_t *account;

	float xpos;
	float ypos;
	float zpos;
	float yrot;
	float urot;
	float speed;

	short energy;

	unsigned char state;

	unsigned char cockpit_type;
	unsigned char engine_type;
	unsigned char addon_type;
	unsigned char shield_type;
	unsigned char pgun_type;
	unsigned char sgun_type;

	ship_info_t info;
} ship_t;

extern ship_t *ship_login (account_t *account);
extern int ship_quit (ship_t *ship);
extern ship_t *ship_find (unsigned short id);
extern int init_ship ();

#endif

