/*
 *  Nothingness - massive multiplayer space action server
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include "network.h"
#include "account.h"
#include "config.h"
#include "client.h"
#include "proto.h"
#include "mysql.h"
#include "ship.h"


int init ()
{
	if (!init_config ())
		return 0;

	if (!init_account ())
		return 0;

	if (!init_mysql ())
		return 0;

	if (!init_ship ())
		return 0;

	if (!init_client ())
		return 0;

	if (!init_proto ())
		return 0;

	if (!init_network ())
		return 0;

	return 1;
}

int loop ()
{
	for ( ; ; )
		network_handler ();
}

int main (int argc, char **argv)
{
	if (!init ()) {
		printf ("ERROR -> init () failed\n");
		return -1;
	}

	loop ();

	return 0;
}
 
