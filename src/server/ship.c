/*
 *  Nothingness - massive multiplayer space action server
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "ship.h"
#include "client.h"

ship_t ship_list;

ship_t *ship_login (account_t *account)
{
	/* alloc and init context */
	ship_t *ship = (ship_t *) malloc (sizeof (ship_t));

	if (!ship)
		return 0;

	ship->account = account;
	ship->info.name = strdup ("N/A");
	ship->info.name_len = strlen (ship->info.name);

	/* add into list */
	ship->next = &ship_list;
	ship->prev = ship_list.prev;
	ship->prev->next = ship;
	ship->next->prev = ship;

	return ship;
}

int ship_quit (ship_t *ship)
{
	free (ship->info.name);

	ship->next->prev = ship->prev;
	ship->prev->next = ship->next;

	free (ship);

	return 1;
}

ship_t *ship_find (unsigned short id)
{
	client_t *client = client_findbyid (id);

	if (!client)
		return 0;

	ship_t *ship = client->ship;

	if (!ship)
		return 0;

	return ship;
}

int init_ship ()
{
	ship_list.next = &ship_list;
	ship_list.prev = &ship_list;

	return 1;
}


 
