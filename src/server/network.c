/*
 *  Nothingness - massive multiplayer space action server
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "network.h"
#include "config.h"
#include "proto.h"
#include "client.h"

NETWORK net;
extern client_t client_list;

int send_to_client (void *client, char *buffer, unsigned len)
{
	client_t *c = client;

	return sendto (net.fd, buffer, len, 0, (struct sockaddr *) &c->sockaddr, c->socklen);
}

int send_to_all (char *buffer, unsigned len)
{

	client_t *t;

	for (t = client_list.next; t != &client_list; t = t->next)
		sendto (net.fd, buffer, len, 0, (struct sockaddr *) &t->sockaddr, t->socklen);

	return 1;
}

int send_to_allex (void *client, char *buffer, unsigned len)
{
	client_t *c = client;
	client_t *t;

	for (t = client_list.next; t != &client_list; t = t->next) {
		if (t != c)
			sendto (net.fd, buffer, len, 0, (struct sockaddr *) &t->sockaddr, t->socklen);
	}

	return 1;
}

int network_handler ()
{
	int ret = recvfrom (net.fd, net.buffer, NETWORK_BUFFER_SIZE, 0, (struct sockaddr *) &net.client, &net.clientlen);

	if (ret > 0) {
		net.buffer[ret] = '\0';

		client_t *client = client_find (&net.client);

		if (!client)
			client = client_register (&net.client);
		
		if (client)
			proto_handler (client, net.buffer, ret);
	}

	usleep (50);

	return 1;
}

int network_listen (int port)
{
	if ((net.fd = socket (AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
		printf ("ERROR -> socket () failed\n");
		return -1;
	}

	net.sockaddr.sin_family = AF_INET;
	net.sockaddr.sin_port = htons (port);
	net.sockaddr.sin_addr.s_addr = INADDR_ANY;

	if (bind (net.fd, (struct sockaddr *) &net.sockaddr, sizeof (net.sockaddr)) == -1) {
		printf ("ERROR -> bind () on port %d failed\n", port);
		return -1;
	}

	/* Set to nonblocking socket mode */
	int oldFlag = fcntl (net.fd, F_GETFL, 0);
	if (fcntl (net.fd, F_SETFL, oldFlag | O_NONBLOCK) == -1) {
		return -1;
	}

	printf ("INFO -> listen on port %d/UDP\n", port);

	return 1;
}

int init_network ()
{
	if (!network_listen (config.port))
		return 0;

	net.buffer = (char *) malloc (sizeof (char) * (NETWORK_BUFFER_SIZE + 1));

	if (!net.buffer)
		return 0;

	return 1;
}


 
