/*
 *  Nothingness - massive multiplayer space action
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _ACCOUNT_H
#define _ACCOUNT_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Ship structure */
typedef struct account_context {
	struct account_context *next, *prev;

	char *account;
	unsigned char ac_len;
	char *password;
	unsigned char pwd_len;

	unsigned char logged;
} account_t;

extern account_t *account_register (char *account, unsigned char ac_len, char *password, unsigned char pwd_len);
extern account_t *account_login (char *account, unsigned char ac_len, char *password, unsigned char pwd_len);
extern int account_delete (account_t *account);
extern account_t *account_find (char *account, unsigned char len);
extern int init_account ();

#endif

