/*
 *  Nothingness - massive multiplayer space action
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "network.h"
#include "scene.h"
#include "config.h"
#include "ship.h"
#include "cache.h"

SDL_mutex *mutex_shipadd;
SDL_mutex *mutex_shipammo;
extern CONFIG config;

using namespace std;

ship_t ship_list; // structure of all ships
ship_t *me;

ship_ammo_t ship_ammo_list;

ship_t *ship_find (unsigned short id)
{
	printf ("ship_find (%d)\n", id);

	ship_t *p;
	for (p = ship_list.next; p != &ship_list; p = p->next)
		if (p->id == id)
			return p;

	return 0;
}

/* alokujeme strukturu pro nasi lod */
ship_t *ship_addself ()
{
	// For communication is used socket "client"
	ship_t *ship;

	// alloc and init context
	ship = (ship_t *) malloc (sizeof (ship_t));

	if (!ship)
		return 0;

	ship->info.name = 0;

	printf ("CLIENT -> Your ship was registered\n");
	
	return ship;
}

/* pridame noveho hrace do structury */
ship_t *ship_add (unsigned short id)
{
	SDL_LockMutex (mutex_shipadd);

	// For communication is used socket "client"
	ship_t *ship;

	// alloc and init context
	ship = (ship_t *) malloc (sizeof (ship_t));

	if (!ship)
		return 0;


	ship->id = id;
	ship->info.name = 0;
	ship->info.name_len = 0;

	char mdl[64];
	char modelfile[32];

	/*switch (ship.cockpit.type) {
		case SHIP_TYPE_DARKWOLF
		ship->cockpit.model = loadmodel ();

	}*/

	strcpy (modelfile, "esta-eco/cockpit");

	sprintf (mdl, "%s%s.3ds", SHIP_MODEL_PATH, modelfile);printf ("hmmm hmm222\n");
	ship->cockpit.model = loadmodel (mdl);

	if (!ship->cockpit.model)
		return 0;

	//sprintf (mdl, SHIP_MODEL_PATH "%s.bmp", modelfile);
	//ship->cockpit.model->texture = loadtexture (mdl);

	// add into list
	ship->next = &ship_list;
	ship->prev = ship_list.prev;
	ship->prev->next = ship;
	ship->next->prev = ship;

	printf ("CLIENT -> New ship with id '%d' connected\n", id);

	SDL_UnlockMutex (mutex_shipadd);
	
	return ship;
}

bool ship_del (ship_t *ship)
{
	printf ("CLIENT -> Ship '%s' was removed from list\n", ship->info.name);

 	ship->next->prev = ship->prev;
 	ship->prev->next = ship->next;
	
	free (ship->info.name);
	free (ship);

	return 1;
}

/* registrujeme naboj hned predtim, nez ho nekdo vystreli */
ship_ammo_t *ship_ammo_register (ship_t *ship)
{
	SDL_LockMutex (mutex_shipammo);

	// For communication is used socket "client"
	ship_ammo_t *ammo;

	// alloc and init context
	ammo = (ship_ammo_t *) malloc (sizeof (ship_ammo_t));

	if (!ammo)
		return 0;

	ammo->owner = ship;

	// add into list
	ammo->next = &ship_ammo_list;
	ammo->prev = ship_ammo_list.prev;
	ammo->prev->next = ammo;
	ammo->next->prev = ammo;

	SDL_UnlockMutex (mutex_shipammo);
	
	return ammo;
}

bool ship_ammo_delete (ship_ammo_t *ammo)
{
	SDL_LockMutex (mutex_shipammo);

 	ammo->next->prev = ammo->prev;
 	ammo->prev->next = ammo->next;

	free (ammo);

	SDL_UnlockMutex (mutex_shipammo);

	return 1;
}

int init_ship ()
{
	ship_list.next = &ship_list;
	ship_list.prev = &ship_list;

	ship_ammo_list.next = &ship_ammo_list;
	ship_ammo_list.prev = &ship_ammo_list;

	mutex_shipadd = SDL_CreateMutex();
	mutex_shipammo = SDL_CreateMutex();

	me = ship_addself ();

	if (!me)
		return 0;

	return 1;
}
