/*
 *  Nothingness - massive multiplayer space action
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "scene.h"
#include "config.h"
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glx.h>


GLuint  base; /* Base Display List For The Font Set */
GLfloat cnt1; /* 1st Counter Used To Move Text & For Coloring */
GLfloat cnt2; /* 2nd Counter Used To Move Text & For Coloring */

GLuint  basegl;                  /* Base Display List For The Font           */

extern SDL_Surface *surface;
extern CONFIG config;
extern GLuint img_font;

//#################################
//-------------------------- font #
//#################################


/* function to recover memory form our list of characters */
GLvoid KillFont (GLvoid)
{
	glDeleteLists (base, 96);
	glDeleteLists (basegl, 256); /* Delete All 256 Display Lists */

	return;
}


GLvoid InitGLFont (GLvoid)
{
	GLuint loop; /* Loop variable               */
	float cx;    /* Holds Our X Character Coord */
	float cy;    /* Holds Our Y Character Coord */

	/* Creating 256 Display List */
	basegl = glGenLists (256);

	/* Select Our Font Texture */
	glBindTexture (GL_TEXTURE_2D, img_font);

	/* Loop Through All 256 Lists */
	for (loop = 0; loop < 256; loop ++) {
		/* NOTE:
		*  BMPs are stored with the top-leftmost pixel being the
		* last byte and the bottom-rightmost pixel being the first
		* byte. So an image that is displayed as
		*    1 0
		*    0 0
		* is represented data-wise like
		*    0 0
		*    0 1
		* And because SDL_LoadBMP loads the raw data without
		* translating to how it is thought of when viewed we need
		* to start at the bottom-right corner of the data and work
		* backwards to get everything properly. So the below code has
		* been modified to reflect this. Examine how this is done and
		* how the original tutorial is done to grasp the differences.
		*
		* As a side note BMPs are also stored as BGR instead of RGB
		* and that is why we load the texture using GL_BGR. It's
		* bass-ackwards I know but whattaya gonna do?
		*/

		/* X Position Of Current Character */
		cx = 1 - ( float )( loop % 16 ) / 16.0f;
		/* Y Position Of Current Character */
		cy = 1 - ( float )( loop / 16 ) / 16.0f;

		/* Start Building A List */
		glNewList (basegl + ( 255 - loop ), GL_COMPILE);

		/* Use A Quad For Each Character */
		glBegin(GL_QUADS);
			  /* Texture Coord (Bottom Left) */
			  glTexCoord2f( cx - 0.0625, cy );
			  /* Vertex Coord (Bottom Left) */
			  glVertex2i( 0, 0 );

			  /* Texture Coord (Bottom Right) */
			  glTexCoord2f( cx, cy );
			  /* Vertex Coord (Bottom Right) */
			  glVertex2i( 16, 0 );

			  /* Texture Coord (Top Right) */
			  glTexCoord2f( cx, cy - 0.0625f );
			  /* Vertex Coord (Top Right) */
			  glVertex2i( 16, 16 );

			  /* Texture Coord (Top Left) */
			  glTexCoord2f( cx - 0.0625f, cy - 0.0625f);
			  /* Vertex Coord (Top Left) */
			  glVertex2i( 0, 16 );
		glEnd( );

		/* Move To The Left Of The Character */
		glTranslated (10, 0, 0);
		glEndList ();
        }
}

/* function to build our font list */
bool InitFont ()
{
	Display *dpy;          /* Our current X display */
	XFontStruct *fontInfo; /* Our font info */

	/* Sotrage for 96 characters */
	base = glGenLists( 96 );

	/* Get our current display long enough to get the fonts */
	dpy = XOpenDisplay( NULL );

	/* Get the font information */
	fontInfo = XLoadQueryFont( dpy, "-adobe-helvetica-medium-r-normal--18-*-*-*-p-*-iso8859-1" );

	/* If the above font didn't exist try one that should */
	if ( fontInfo == NULL )
	{
		fontInfo = XLoadQueryFont( dpy, "fixed" );
		/* If that font doesn't exist, something is wrong */
		if ( fontInfo == NULL )
		{
		    fprintf( stderr, "no X font available?\n" );
		    return 0;
		}
	}

	/* generate the list */
	glXUseXFont( fontInfo->fid, 32, 96, base );

	/* Recover some memory */
	XFreeFont( dpy, fontInfo );

	/* close the display now that we're done with it */
	XCloseDisplay( dpy );

	InitGLFont ();

	return 1;
}

/* Print our GL text to the screen */
GLvoid glPrint (const char *fmt, ...)
{
	char text[256]; /* Holds our string */
	va_list ap;     /* Pointer to our list of elements */

	/* If there's no text, do nothing */
	if (fmt == NULL)
		return;

	/* Parses The String For Variables */
	va_start (ap, fmt);
		/* Converts Symbols To Actual Numbers */
		vsprintf(text, fmt, ap);
	va_end (ap);

	/* Pushes the Display List Bits */
	glPushAttrib (GL_LIST_BIT);

	/* Sets base character to 32 */
	glListBase (base - 32);

	/* Draws the text */
	glCallLists (strlen(text), GL_UNSIGNED_BYTE, text);

	/* Pops the Display List Bits */
	glPopAttrib ();
}

/* Function to print the string */
extern unsigned screen_x, screen_y, screen_bpp;
GLvoid glPrint3f (float x, float y, float z, float size, char *string, int set)
{
    if ( set > 1 )
	set = 1;

    /* Select our texture */
    glBindTexture( GL_TEXTURE_2D, img_font );

    /* Disable depth testing */
    glDisable( GL_DEPTH_TEST );

    /* Select The Projection Matrix */
//    glMatrixMode( GL_PROJECTION );
    /* Store The Projection Matrix */
    glPushMatrix( );

	glBlendFunc( GL_SRC_ALPHA, GL_ONE );
	glEnable( GL_BLEND );

    glPushMatrix( );
    /* Reset The Modelview Matrix */
//    glLoadIdentity( );

    /* Position The Text (0,0 - Bottom Left) */
    glTranslatef ( x, y, z );

    glScalef (size, size, size);
    /* Choose The Font Set (0 or 1) */
    glListBase( basegl - 32 + ( 128 * set ) );

    /* Write The Text To The Screen */
    glCallLists( strlen( string ), GL_BYTE, string );

    /* Select The Projection Matrix */
    //glMatrixMode( GL_PROJECTION );
    /* Restore The Old Projection Matrix */
    glPopMatrix( );

    /* Select the Modelview Matrix */
    //glMatrixMode( GL_MODELVIEW );
    /* Restore the Old Projection Matrix */
	glDisable( GL_BLEND );
    glPopMatrix( );

    /* Re-enable Depth Testing */
    glEnable( GL_DEPTH_TEST );
}
