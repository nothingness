#!/bin/bash
if [ "$1" = "clean" ] ; then
	echo "Cleaning source from binary files .."
	cd src && make clean; cd ..
	echo "OK"
	exit 1
fi

cd src && make; cd ..

